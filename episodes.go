package transistor

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"time"
)

// Episode represents a single podcast episode.
//
// See https://developers.transistor.fm/#Episode
type Episode struct {
	ID           string `json:"id" url:"-"`
	AlternateURL string `json:"alternate_url" url:"alternate_url,omitempty"`
	// TranscriptURL is the URL To the transcript text, used for reading a
	// transcript.
	TranscriptURL string `json:"transcript_url" url:"-"`
	// TranscriptText is the full text of the transcript, only used for setting
	// the transcript. Use TranscriptURL to read the existing transcript.
	TranscriptText  string    `json:"-" url:"transcript_text"`
	AudioProcessing bool      `json:"audio_processing" url:"-"`
	Author          string    `json:"author" url:"author,omitempty"`
	CreatedAt       time.Time `json:"created_at" url:"-"`
	Description     string    `json:"description" url:"description,omitempty"`
	// Duration of the episode in seconds.
	Duration int `json:"duration" url:"-"`
	// FormattedDuration is the duration of episode in minutes and seconds.
	// e.g. 34:12
	FormattedDuration  string `json:"duration_in_mmss" url:"-"`
	EmailNotifications string `json:"email_notifications" url:"email_notifications,omitempty"`
	EmbedHTML          string `json:"embed_html" url:"-"`
	EmbedHTMLDark      string `json:"embed_html_dark" url:"-"`
	Explicit           bool   `json:"explicit" url:"explicit,omitempty"`
	// Formatted version of the PublishedAt field.
	FormattedPublishedAt string `json:"formatted_published_at" url:"-"`
	// FormattedSummary is the formatted episode summary short description.
	FormattedSummary string `json:"formatted_summary" url:"-"`
	ImageURL         string `json:"image_url" url:"image_url,omitempty"`
	Keywords         string `json:"keywords" url:"keywords,omitempty"`
	// MediaURL is the URL to the trackable audio MP3 file.
	MediaURL string `json:"media_url" url:"-"`
	// Number is the episode number.
	Number int `json:"number" url:"number,omitempty"`
	// PublishedAt is the time the episode was published, in the podcast's
	// time zone.
	PublishedAt time.Time `json:"published_at" url:"-"`
	// Season number.
	Season    int       `json:"season" url:"season,omitempty"`
	ShareURL  string    `json:"share_url" url:"-"`
	Status    string    `json:"status" url:"-"`
	Summary   string    `json:"summary" url:"summary,omitempty"`
	Title     string    `json:"title" url:"title,omitempty"`
	Type      string    `json:"type" url:"type,omitempty"`
	UpdatedAt time.Time `json:"updated_at" url:"-"`
	// ShowID is the id of the show this episode belongs to.
	ShowID string `json:"-" url:"show_id,omitempty"`
}

// UnmarshalJSON satisfies the json.Unmarshaler interface.
func (e *Episode) UnmarshalJSON(p []byte) error {
	target := struct {
		*Episode
		Attributes    json.RawMessage `json:"attributes"`
		Relationships json.RawMessage `json:"relationships"`
		UnmarshalJSON struct{}        `json:"-"`
	}{
		Episode: e,
	}
	if err := json.Unmarshal(p, &target); err != nil {
		return err
	}
	if len(target.Attributes) > 0 {
		if err := json.Unmarshal(target.Attributes, &target); err != nil {
			return err
		}
	}
	if len(target.Relationships) > 0 {
		rels := new(relationships)
		if err := json.Unmarshal(target.Relationships, &rels); err != nil {
			return err
		}
		e.ShowID = rels.get("show")[0]
	}
	return nil
}

// EpisodesQuery is a query for episodes.
type EpisodesQuery struct {
	ShowID     string `url:"show_id"`
	Query      string `url:"query,omitempty"`
	Status     string `url:"status"`
	Pagination `url:"pagination"`
	Fields     map[string][]string `url:"fields"`
}

// Episodes retrieves a paginated list of episodes in descending order by
// published date.
//
// See https://developers.transistor.fm/#episodes
func (c *Client) Episodes(ctx context.Context, query *EpisodesQuery) ([]*Episode, *Meta, error) {
	response := new(struct {
		Data []*Episode `json:"data"`
		Meta *Meta      `json:"meta"`
	})
	err := c.doRequest(ctx, http.MethodGet, c.path("episodes").query(query), nil, response)
	if err != nil {
		return nil, nil, err
	}
	return response.Data, response.Meta, nil
}

// Episode retrieves a single podcast episode.
//
// See https://developers.transistor.fm/#episodes
func (c *Client) Episode(ctx context.Context, id string) (*Episode, error) {
	response := new(struct {
		Data *Episode `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodGet, c.path("episodes", id), nil, response)
	return response.Data, err
}

// AudioUpload represents an authorized audio upload resource.
//
// See https://developers.transistor.fm/#AudioUpload
type AudioUpload struct {
	ID string `json:"id"`
	// UploadURL is the endpoint URL to upload your audio file using HTTP PUT.
	UploadURL string `json:"upload_url"`
	// ContentType ist he content type of the file to upload. audio/mpeg,
	// audio/wav, etc.
	ContentType string `json:"content_type"`
	// Expiration time of this link in seconds.
	ExpiresIn int `json:"expires_in"`
	// AudioURL is the URL of your audio file after uploading is complete,
	// to be used when creating or updating an episode.
	AudioURL string `json:"audio_url"`
}

// UnmarshalJSON satisfies the json.Unmarshaler interface.
func (a *AudioUpload) UnmarshalJSON(p []byte) error {
	target := struct {
		*AudioUpload
		Attributes    json.RawMessage `json:"attributes"`
		Relationships json.RawMessage `json:"relationships"`
		UnmarshalJSON struct{}        `json:"-"`
	}{
		AudioUpload: a,
	}
	if err := json.Unmarshal(p, &target); err != nil {
		return err
	}
	if len(target.Attributes) > 0 {
		if err := json.Unmarshal(target.Attributes, &target); err != nil {
			return err
		}
	}
	return nil
}

// AuthorizeUpload authorizes a URL for uploading an audio file to be used when
// creating or updating an episode, if you don't already have a URL for an audio
// file.
//
// See https://developers.transistor.fm/#episodes
func (c *Client) AuthorizeUpload(ctx context.Context, filename string) (*AudioUpload, error) {
	request := struct {
		Filename string `url:"filename"`
	}{
		Filename: filename,
	}
	response := new(struct {
		Data *AudioUpload `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodGet, c.path("episodes/authorize_upload").query(request), nil, response)
	return response.Data, err
}

// CreateEpisode create a new draft episode for the specified show. Note that
// publishing an episode involves a separate endpoint.
//
// See https://developers.transistor.fm/#episodes
func (c *Client) CreateEpisode(ctx context.Context, episode *Episode) (*Episode, error) {
	if episode == nil {
		return nil, errors.New("episode required")
	}
	if episode.ShowID == "" {
		return nil, errors.New("episode.ShowID required")
	}
	request := struct {
		Episode *Episode `url:"episode"`
	}{
		Episode: episode,
	}
	response := new(struct {
		Data *Episode `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodPost, c.path("episodes"), request, response)
	return response.Data, err
}

// UpdateEpisode Update a single podcast episode. Note that publishing or
// unpublishing an episode involves a separate endpoint.
//
// See https://developers.transistor.fm/#episodes
func (c *Client) UpdateEpisode(ctx context.Context, episode *Episode) (*Episode, error) {
	if episode == nil {
		return nil, errors.New("episode required")
	}
	if episode.ID == "" {
		return nil, errors.New("episode.ID required")
	}
	request := struct {
		Episode *Episode `url:"episode"`
	}{Episode: episode}
	response := new(struct {
		Data *Episode `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodPatch, c.path("episodes", episode.ID), request, response)
	return response.Data, err
}

type publish struct {
	Status      string    `url:"status"`
	PublishedAt time.Time `url:"published_at,omitempty"`
}

// PublishEpisode pulibshes a single episode now or in the past, schedule for
// the future, or revert to a draft.
//
// publishAt defaults to now.
//
// See https://developers.transistor.fm/#episodes
func (c *Client) PublishEpisode(ctx context.Context, id, status string, publishAt time.Time) (*Episode, error) {
	if id == "" {
		return nil, errors.New("id required")
	}
	request := struct {
		Episode *publish `url:"episode"`
	}{
		Episode: &publish{
			Status:      status,
			PublishedAt: publishAt,
		},
	}
	response := new(struct {
		Data *Episode `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodPatch, c.path("episodes", id, "publish"), request, response)
	return response.Data, err
}
