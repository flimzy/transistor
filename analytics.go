package transistor

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"time"
)

// ShowAnalytics reports download analytics per day for a single podcast.
//
// See https://developers.transistor.fm/#ShowAnalytics
type ShowAnalytics struct {
	ID        string      `json:"id"`
	Downloads []Downloads `json:"downloads"`
	ShowID    string      `json:"-"`
	StartDate time.Time   `json:"start_date"`
	EndDate   time.Time   `json:"end_date"`
}

// UnmarshalJSON satisfies the json.Unmarshaler interface.
func (s *ShowAnalytics) UnmarshalJSON(p []byte) error {
	target := struct {
		*ShowAnalytics
		Attributes    json.RawMessage `json:"attributes"`
		Relationships json.RawMessage `json:"relationships"`
		StartDate     dateMDY         `json:"start_date"`
		EndDate       dateMDY         `json:"end_date"`
		UnmarshalJSON struct{}        `json:"-"`
	}{
		ShowAnalytics: s,
	}
	if err := json.Unmarshal(p, &target); err != nil {
		return err
	}
	s.StartDate = time.Time(target.StartDate)
	s.EndDate = time.Time(target.EndDate)
	if len(target.Attributes) > 0 {
		if err := json.Unmarshal(target.Attributes, &target); err != nil {
			return err
		}
	}
	showRels, err := unmarshalRelationships(target.Relationships, "show")
	if err != nil {
		return err
	}
	s.ShowID = showRels[0]
	return nil
}

// Downloads reports the number of downloads on a given day.
type Downloads struct {
	Date      time.Time `json:"date"`
	Downloads int64     `json:"downloads"`
}

// UnmarshalJSON handles custom unmarshaling for
func (d *Downloads) UnmarshalJSON(p []byte) error {
	target := struct {
		*Downloads
		Date          dateDMY  `json:"date"`
		UnmarshalJSON struct{} `json:"-"`
	}{
		Downloads: d,
	}
	if err := json.Unmarshal(p, &target); err != nil {
		return err
	}
	d.Date = time.Time(target.Date)
	return nil
}

// downloadsMDY duplicates Downloads, but for dateMDY-formatted dates.
type downloadsMDY struct {
	Date      time.Time `json:"date"`
	Downloads int64     `json:"downloads"`
}

// UnmarshalJSON handles custom unmarshaling for
func (d *downloadsMDY) UnmarshalJSON(p []byte) error {
	target := struct {
		*downloadsMDY
		Date          dateMDY  `json:"date"`
		UnmarshalJSON struct{} `json:"-"`
	}{
		downloadsMDY: d,
	}
	if err := json.Unmarshal(p, &target); err != nil {
		return err
	}
	d.Date = time.Time(target.Date)
	return nil
}

// AnalyticsQuery is a query for ShowAnalytics, EpisodeAnalytics, or
// AllEpisodesAnalytics.
//
// See https://developers.transistor.fm/#analytics
type AnalyticsQuery struct {
	StartDate time.Time           `url:"start_date,omitempty,format=02-01-2006"`
	EndDate   time.Time           `url:"end_date,omitempty,format=02-01-2006"`
	Fields    map[string][]string `url:"fields,omitempty"`
	Include   []string            `url:"include,omitempty"`
}

// ShowAnalytics retrieves analytics of downloads per day for an entire
// podcast. Defaults to the4 last 14 days.
//
// Only the first query, if provided, is honored.
//
// See https://developers.transistor.fm/#analytics
func (c *Client) ShowAnalytics(ctx context.Context, showID string, query ...*AnalyticsQuery) (*ShowAnalytics, error) {
	if showID == "" {
		return nil, errors.New("query.ShowID required")
	}
	response := new(struct {
		Data *ShowAnalytics `json:"data"`
	})
	var q interface{}
	if len(query) > 0 {
		q = query[0]
	}
	err := c.doRequest(ctx, http.MethodGet, c.path("analytics", showID).query(q), nil, response)
	return response.Data, err
}

// EpisodesAnalytics is a report of download analytics per day for all episodes
// of a single podcast.
//
// See https://developers.transistor.fm/#EpisodesAnalytics
type EpisodesAnalytics struct {
	ID        string             `json:"id"`
	Episodes  []EpisodeAnalytics `json:"episodes"`
	StartDate time.Time          `json:"start_date"`
	EndDate   time.Time          `json:"end_date"`
	ShowID    string             `json:"-"`
}

// UnmarshalJSON satisfies the json.Unmarshaler interface.
func (a *EpisodesAnalytics) UnmarshalJSON(p []byte) error {
	target := struct {
		*EpisodesAnalytics
		Attributes    json.RawMessage `json:"attributes"`
		Relationships json.RawMessage `json:"relationships"`
		StartDate     dateMDY         `json:"start_date"`
		EndDate       dateMDY         `json:"end_date"`
		UnmarshalJSON struct{}        `json:"-"`
	}{
		EpisodesAnalytics: a,
	}
	if err := json.Unmarshal(p, &target); err != nil {
		return err
	}
	a.StartDate = time.Time(target.StartDate)
	a.EndDate = time.Time(target.EndDate)
	if len(target.Attributes) > 0 {
		if err := json.Unmarshal(target.Attributes, &target); err != nil {
			return err
		}
	}
	showRels, err := unmarshalRelationships(target.Relationships, "show")
	if err != nil {
		return err
	}
	a.ShowID = showRels[0]
	return nil
}

// EpisodeAnalytics reports download analytics per day for a single episode.
//
// See https://developers.transistor.fm/#EpisodeAnalytics
type EpisodeAnalytics struct {
	ID        string      `json:"id"`
	Downloads []Downloads `json:"downloads"`
	StartDate time.Time   `json:"start_date"`
	EndDate   time.Time   `json:"end_date"`
}

// UnmarshalJSON handles the backwards parsing of dates in the Downloads slice.
func (a *EpisodeAnalytics) UnmarshalJSON(p []byte) error {
	var target struct {
		ID        stringOrInt    `json:"id"`
		Downloads []downloadsMDY `json:"downloads"`
		StartDate dateMDY        `json:"start_date"`
		EndDate   dateMDY        `json:"end_date"`
	}
	if err := json.Unmarshal(p, &target); err != nil {
		return err
	}
	a.ID = string(target.ID)
	a.StartDate = time.Time(target.StartDate)
	a.EndDate = time.Time(target.EndDate)
	a.Downloads = make([]Downloads, len(target.Downloads))
	for i, downloads := range target.Downloads {
		a.Downloads[i] = Downloads(downloads)
	}
	return nil
}

// AllEpisodesAnalytics retrieves analytics of downloads for all episodes of a podcast.
// Defaults to the last 7 days.
//
// Only the first query value is honored.
//
// See https://developers.transistor.fm/#analytics
func (c *Client) AllEpisodesAnalytics(ctx context.Context, showID string, query ...*AnalyticsQuery) (*EpisodesAnalytics, error) {
	if showID == "" {
		return nil, errors.New("query.ShowID required")
	}
	response := new(struct {
		Data *EpisodesAnalytics `json:"data"`
	})
	var q interface{}
	if len(query) > 0 {
		q = query[0]
	}
	err := c.doRequest(ctx, http.MethodGet, c.path("analytics", showID, "episodes").query(q), nil, response)
	return response.Data, err
}

// EpisodeAnalytics retrieves analytics of downloads for a single episode.
// Defaults to the last 14 days.
//
// Only the first query value is honored.
//
// See https://developers.transistor.fm/#analytics
func (c *Client) EpisodeAnalytics(ctx context.Context, episodeID string, query ...*AnalyticsQuery) (*EpisodeAnalytics, error) {
	if episodeID == "" {
		return nil, errors.New("query.ShowID required")
	}
	response := new(struct {
		Data *singleEpisodeAnalytics `json:"data"`
	})
	var q interface{}
	if len(query) > 0 {
		q = query[0]
	}
	err := c.doRequest(ctx, http.MethodGet, c.path("analytics/episodes", episodeID).query(q), nil, response)
	if err != nil {
		return nil, err
	}
	a := EpisodeAnalytics(*response.Data)
	return &a, nil
}

// singleEpisodeAnalytics is a duplicate of EpisodesAnalytics, but handles the
// attributes, and yet again another reversed sense of date formatting.
type singleEpisodeAnalytics struct {
	ID        string      `json:"id"`
	Downloads []Downloads `json:"downloads"`
	StartDate time.Time   `json:"start_date"`
	EndDate   time.Time   `json:"end_date"`
}

// UnmarshalJSON handles the backwards parsing of dates in the Downloads slice.
func (a *singleEpisodeAnalytics) UnmarshalJSON(p []byte) error {
	target := struct {
		*singleEpisodeAnalytics
		StartDate     dateMDY         `json:"start_date"`
		EndDate       dateMDY         `json:"end_date"`
		Attributes    json.RawMessage `json:"attributes"`
		UnmarshalJSON struct{}        `json:"-"`
	}{
		singleEpisodeAnalytics: a,
	}
	if err := json.Unmarshal(p, &target); err != nil {
		return err
	}
	if len(target.Attributes) > 0 {
		if err := json.Unmarshal(target.Attributes, &target); err != nil {
			return err
		}
	}
	a.StartDate = time.Time(target.StartDate)
	a.EndDate = time.Time(target.EndDate)
	return nil
}
