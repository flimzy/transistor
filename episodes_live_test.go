package transistor

import (
	"context"
	"testing"
	"time"
)

func TestEpisodes_live(t *testing.T) {
	t.Parallel()
	c := liveClient(t)
	episodes, _, err := c.Episodes(context.Background(), nil)
	if err != nil {
		t.Fatal(err)
	}
	if len(episodes) > 1 {
		t.Errorf("Wanted 1 show, got %d", len(episodes))
	}
}

func TestEpisode_live(t *testing.T) {
	t.Parallel()
	c := liveClient(t)
	shows, err := c.Episode(context.Background(), testEpisodeID)
	if err != nil {
		t.Fatal(err)
	}
	if shows.Title != "Pilot" {
		t.Errorf("Unexpected title: %s", shows.Title)
	}
}

func TestAuthorizeUpload_live(t *testing.T) {
	t.Parallel()
	c := liveClient(t)
	upload, err := c.AuthorizeUpload(context.Background(), "foo.mp3")
	if err != nil {
		t.Fatal(err)
	}
	if upload.ExpiresIn != 600 {
		t.Errorf("Unexpected expiry: %v", upload.ExpiresIn)
	}
}

func TestCreateEpisode_live(t *testing.T) {
	t.Skip("No way to automatically delete the episodes we create")
	t.Parallel()
	t.Run("success", func(t *testing.T) {
		t.Parallel()
		t.Skip()
		c := liveClient(t)
		c.debug = true
		episode, err := c.CreateEpisode(context.Background(), &Episode{
			ShowID: testShowID,
			Title:  "new test episode",
		})
		if err != nil {
			t.Fatal(err)
		}
		if episode.Title != "new test episode" {
			t.Errorf("unexpected title: %s", episode.Title)
		}
	})
	t.Run("with transcript", func(t *testing.T) {
		t.Parallel()
		c := liveClient(t)
		c.debug = true
		episode, err := c.CreateEpisode(context.Background(), &Episode{
			ShowID:         testShowID,
			Title:          "new test episode",
			TranscriptText: "This is a test transcript",
		})
		if err != nil {
			t.Fatal(err)
		}
		if episode.Title != "new test episode" {
			t.Errorf("unexpected title: %s", episode.Title)
		}
	})
}

func TestUpdateEpisode_live(t *testing.T) {
	t.Parallel()
	c := liveClient(t)
	episode := &Episode{
		ID:    testEpisodeID,
		Title: "Pilot",
	}
	episode, err := c.UpdateEpisode(context.Background(), episode)
	if err != nil {
		t.Fatal(err)
	}
	if episode.Title != "Pilot" {
		t.Errorf("Unepxected title: %s", episode.Title)
	}
}

func TestPublishEpisode_live(t *testing.T) {
	t.Parallel()
	c := liveClient(t)
	episode, err := c.PublishEpisode(context.Background(), testEpisodeID, "draft", time.Time{})
	if err != nil {
		t.Fatal(err)
	}
	if episode.Title != "Pilot" {
		t.Errorf("Unepxected title: %s", episode.Title)
	}
}
