package transistor

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"time"
)

// ShowsQuery is a query for shows.
type ShowsQuery struct {
	// Private filters for private shows.
	Private    bool   `url:"private,omitempty"`
	Query      string `url:"query,omitempty"`
	Pagination `url:"pagination,omitempty"`
	Fields     map[string][]string `url:"fields,omitempty"`
}

// Show represents a show.
//
// See https://developers.transistor.fm/#Show
type Show struct {
	ID                 string    `json:"id" url:"-"`
	AmazonMusicURL     string    `json:"amazon_music" url:"-"`
	ApplePodcastsURL   string    `json:"apple_podcasts" url:"-"`
	Author             string    `json:"author" url:"author,omitempty"`
	CastboxURL         string    `json:"castbox" url:"-"`
	CastroURL          string    `json:"castro" url:"-"`
	Category           string    `json:"category" url:"category,omitempty"`
	Copyright          string    `json:"copyright" url:"copyright,omitempty"`
	CreatedAt          time.Time `json:"created_at" url:"-"`
	DeezerURL          string    `json:"deezer" url:"-"`
	Description        string    `json:"description" url:"description,omitempty"`
	EmailNotifications bool      `json:"email_notifications" url:"-"`
	Explicit           bool      `json:"explicit" url:"explicit,omitempty"`
	FeedURL            string    `json:"feed_url" url:"-"`
	GooglePodcastsURL  string    `json:"google_podcasts" url:"-"`
	IHeartRadioURL     string    `json:"iHeartRadio" url:"-"`
	ImageURL           string    `json:"image_url" url:"-"`
	// Comma-separated list of keywords.
	Keywords string `json:"keywords"  url:"keywords,omitempty"`
	// Podcast's spoken language
	Language              string `json:"language" url:"language,omitempty"`
	MultipleSeasons       bool   `json:"multiple_seasons" url:"-"`
	OvercastURL           string `json:"overcast" url:"-"`
	OwnerEmail            string `json:"owner_email" url:"owner_email,omitempty"`
	PandoraURL            string `json:"pandora" url:"-"`
	PasswordProtectedFeed bool   `json:"password_protected_feed" url:"-"`
	PlayerFMURL           string `json:"player_FM" url:"-"`
	// Playlist embed player episode limit.
	PlaylistLimit     int       `json:"playlist_limit" url:"-"`
	PocketCastsURL    string    `json:"pocket_casts" url:"-"`
	Private           bool      `json:"private" url:"-"`
	RadioPublicURL    string    `json:"radioPublic" url:"-"`
	SecondaryCategory string    `json:"secondary_category" url:"secondary_category,omitempty"`
	ShowType          string    `json:"show_type" url:"show_type,omitempty"`
	Slug              string    `json:"slug" url:"-"`
	SoundcloudURL     string    `json:"soundcloud" url:"-"`
	SpotifyURL        string    `json:"spotify" url:"-"`
	StitcherURL       string    `json:"stitcher" url:"-"`
	TimeZone          string    `json:"time_zone" url:"time_zone,omitempty"`
	Title             string    `json:"title" url:"title,omitempty"`
	TuneInURL         string    `json:"tuneIn" url:"-"`
	UpdatedAt         time.Time `json:"updated_at" url:"-"`
	WebsiteURL        string    `json:"website" url:"website,omitempty"`
	Episodes          []string  `json:"-"`
}

// UnmarshalJSON satisfies the json.Unmarshaler interface.
func (s *Show) UnmarshalJSON(p []byte) error {
	target := struct {
		*Show
		Attributes    json.RawMessage `json:"attributes"`
		Relationships json.RawMessage `json:"relationships"`
		UnmarshalJSON struct{}        `json:"-"`
	}{
		Show: s,
	}
	if err := json.Unmarshal(p, &target); err != nil {
		return err
	}
	if len(target.Attributes) > 0 {
		if err := json.Unmarshal(target.Attributes, &target); err != nil {
			return err
		}
	}
	if len(target.Relationships) > 0 {
		rels := new(relationships)
		if err := json.Unmarshal(target.Relationships, &rels); err != nil {
			return err
		}
		s.Episodes = rels.get("episodes")
	}
	return nil
}

// Shows retrieves a paginated list of shows in descending order by updated date.
//
// See https://developers.transistor.fm/#shows
func (c *Client) Shows(ctx context.Context, query *ShowsQuery) ([]*Show, *Meta, error) {
	response := new(struct {
		Data []*Show `json:"data"`
		Meta *Meta   `json:"meta"`
	})
	err := c.doRequest(ctx, http.MethodGet, c.path("shows").query(query), nil, response)
	return response.Data, response.Meta, err
}

// Show retrieves a single show (podcast).
//
// See https://developers.transistor.fm/#shows
func (c *Client) Show(ctx context.Context, id string) (*Show, error) {
	response := new(struct {
		Data *Show `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodGet, c.path("shows", id), nil, response)
	return response.Data, err
}

// UpdateShow updates a show with any of the set attributes. show.ID or show.Slug
// must be set.
//
// See https://developers.transistor.fm/#shows
func (c *Client) UpdateShow(ctx context.Context, show *Show) (*Show, error) {
	if show == nil {
		return nil, errors.New("show required")
	}
	id := show.ID
	if id == "" {
		id = show.Slug
	}
	if id == "" {
		return nil, errors.New("show.ID or show.Slug required")
	}
	request := struct {
		Show *Show `url:"show"`
	}{Show: show}
	response := new(struct {
		Data *Show `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodPatch, c.path("shows", id), request, response)
	return response.Data, err
}
