package transistor

import (
	"context"
	"net/http"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/flimzy/ale/errors"
	"gitlab.com/flimzy/testy"
)

func TestShows(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder testy.HTTPResponder
		query     *ShowsQuery
		err       string
		status    int
		want      []*Show
		wantMeta  *Meta
	}

	tests := testy.NewTable()
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusOK, `{"data":[{"id":"20739","type":"show","attributes":{"author":"Jonathan Hall",
		"category":"Technology","copyright":"© 2022 Jonathan Hall","created_at":"2021-04-28T18:54:18.263Z",
		"description":"Solving big problems with small teams","explicit":false,
		"image_url":"https://images.transistor.fm/file/transistor/images/show/20739/1619720401-artwork.jpg",
		"keywords":"devops,software development,programming,continuous delivery,continuous integration,cicd","language":"en",
		"multiple_seasons":false,"owner_email":"jonathan@jhall.io","playlist_limit":25,"private":false,
		"secondary_category":"Education :: How To","show_type":"episodic","slug":"tinydevops","time_zone":"Amsterdam",
		"title":"Tiny DevOps","updated_at":"2022-10-11T09:55:00.804Z","website":"https://jhall.io/","password_protected_feed":false,
		"castbox":"https://castbox.fm/vic/1567761546","castro":"https://castro.fm/itunes/1567761546",
		"feed_url":"https://feeds.transistor.fm/tinydevops",
		"google_podcasts":"https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL3RpbnlkZXZvcHM=",
		"iHeartRadio":"","overcast":"https://overcast.fm/itunes1567761546","pandora":"",
		"pocket_casts":"https://pca.st/itunes/1567761546","radioPublic":"https://radiopublic.com/tiny-devops-6BZx5R","soundcloud":"","stitcher":"https://www.stitcher.com/podcast/tiny-devops",
		"tuneIn":"https://tunein.com/podcasts/Technology-Podcasts/Tiny-DevOps-p1437848/",
		"spotify":"https://open.spotify.com/show/2BEDe8fGJ8dn5LEk18MKDW",
		"apple_podcasts":"https://podcasts.apple.com/us/podcast/tiny-devops/id1567761546",
		"deezer":"https://www.deezer.com/show/2641062",
		"amazon_music":"https://music.amazon.com/podcasts/e3228372-fa03-48f9-969f-52734df5623c",
		"player_FM":"https://player.fm/series/tiny-devops",
		"podcast_addict":"https://podcastaddict.com/podcast/3337859","email_notifications":false},
		"relationships":{"episodes":{"data":[{"id":"1054248","type":"episode"},{"id":"1037180","type":"episode"},{"id":"1020224",
		"type":"episode"},{"id":"1020917","type":"episode"},{"id":"878294","type":"episode"},{"id":"871212","type":"episode"},
		{"id":"865774","type":"episode"},{"id":"844846","type":"episode"},{"id":"843612","type":"episode"},{"id":"838643",
		"type":"episode"},{"id":"830226","type":"episode"},{"id":"823070","type":"episode"},{"id":"814614","type":"episode"},
		{"id":"811667","type":"episode"},{"id":"799800","type":"episode"},{"id":"793608","type":"episode"},{"id":"785434",
		"type":"episode"},{"id":"779293","type":"episode"},{"id":"777970","type":"episode"},{"id":"767666","type":"episode"},
		{"id":"764016","type":"episode"},{"id":"756382","type":"episode"},{"id":"745347","type":"episode"},{"id":"740745",
		"type":"episode"},{"id":"734040","type":"episode"},{"id":"727059","type":"episode"},{"id":"722080","type":"episode"},
		{"id":"721148","type":"episode"},{"id":"710851","type":"episode"},{"id":"705092","type":"episode"},{"id":"710849",
		"type":"episode"},{"id":"676283","type":"episode"},{"id":"621509","type":"episode"},{"id":"615528","type":"episode"},
		{"id":"609064","type":"episode"},{"id":"602861","type":"episode"},{"id":"597141","type":"episode"},{"id":"591331",
		"type":"episode"},{"id":"586286","type":"episode"},{"id":"578309","type":"episode"},{"id":"574094","type":"episode"},
		{"id":"568388","type":"episode"},{"id":"560106","type":"episode"},{"id":"548717","type":"episode"},{"id":"532348",
		"type":"episode"},{"id":"532211","type":"episode"},{"id":"531539","type":"episode"},{"id":"542498","type":"episode"}]}}}],
		"meta":{"currentPage":1,"totalPages":1,"totalCount":1}}`),
		want: []*Show{
			{
				ID:                "20739",
				AmazonMusicURL:    "https://music.amazon.com/podcasts/e3228372-fa03-48f9-969f-52734df5623c",
				ApplePodcastsURL:  "https://podcasts.apple.com/us/podcast/tiny-devops/id1567761546",
				Author:            "Jonathan Hall",
				CastboxURL:        "https://castbox.fm/vic/1567761546",
				CastroURL:         "https://castro.fm/itunes/1567761546",
				Category:          "Technology",
				Copyright:         "© 2022 Jonathan Hall",
				CreatedAt:         parseTime("2021-04-28T18:54:18.263Z"),
				DeezerURL:         "https://www.deezer.com/show/2641062",
				Description:       "Solving big problems with small teams",
				FeedURL:           "https://feeds.transistor.fm/tinydevops",
				GooglePodcastsURL: "https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL3RpbnlkZXZvcHM=",
				ImageURL:          "https://images.transistor.fm/file/transistor/images/show/20739/1619720401-artwork.jpg",
				Keywords:          "devops,software development,programming,continuous delivery,continuous integration,cicd",
				Language:          "en",
				OvercastURL:       "https://overcast.fm/itunes1567761546",
				OwnerEmail:        "jonathan@jhall.io",
				PlayerFMURL:       "https://player.fm/series/tiny-devops",
				PlaylistLimit:     25,
				RadioPublicURL:    "https://radiopublic.com/tiny-devops-6BZx5R",
				SecondaryCategory: "Education :: How To",
				Slug:              "tinydevops",
				ShowType:          "episodic",
				SpotifyURL:        "https://open.spotify.com/show/2BEDe8fGJ8dn5LEk18MKDW",
				StitcherURL:       "https://www.stitcher.com/podcast/tiny-devops",
				TimeZone:          "Amsterdam",
				Title:             "Tiny DevOps",
				TuneInURL:         "https://tunein.com/podcasts/Technology-Podcasts/Tiny-DevOps-p1437848/",
				UpdatedAt:         parseTime("2022-10-11T09:55:00.804Z"),
				WebsiteURL:        "https://jhall.io/",
				PocketCastsURL:    "https://pca.st/itunes/1567761546",
				Episodes: []string{
					"1054248", "1037180", "1020224", "1020917", "878294",
					"871212", "865774", "844846", "843612", "838643", "830226",
					"823070", "814614", "811667", "799800", "793608", "785434",
					"779293", "777970", "767666", "764016", "756382", "745347",
					"740745", "734040", "727059", "722080", "721148", "710851",
					"705092", "710849", "676283", "621509", "615528", "609064",
					"602861", "597141", "591331", "586286", "578309", "574094",
					"568388", "560106", "548717", "532348", "532211", "531539",
					"542498",
				},
			},
		},
		wantMeta: &Meta{
			CurrentPage: 1,
			TotalPages:  1,
			TotalCount:  1,
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, pag, err := c.Shows(context.Background(), tt.query)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
		if d := cmp.Diff(tt.wantMeta, pag); d != "" {
			t.Errorf("Pagination: %s\n", d)
		}
	})
}

func TestShow(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder testy.HTTPResponder
		id        string
		err       string
		status    int
		want      *Show
	}

	tests := testy.NewTable()
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusOK, `{"data":{"id":"20739","type":"show",
		"attributes":{"author":"Jonathan Hall","category":"Technology",
		"copyright":"© 2022 Jonathan Hall","created_at":"2021-04-28T18:54:18.263Z",
		"description":"Solving big problems with small teams","explicit":false,
		"image_url":"https://images.transistor.fm/file/transistor/images/show/20739/1619720401-artwork.jpg",
		"keywords":"devops,software development,programming,continuous delivery,continuous integration,cicd",
		"language":"en","multiple_seasons":false,"owner_email":"jonathan@jhall.io",
		"playlist_limit":25,"private":false,"secondary_category":"Education :: How To",
		"show_type":"episodic","slug":"tinydevops","time_zone":"Amsterdam","title":"Tiny DevOps",
		"updated_at":"2022-10-11T09:55:00.804Z","website":"https://jhall.io/",
		"password_protected_feed":false,"castbox":"https://castbox.fm/vic/1567761546",
		"castro":"https://castro.fm/itunes/1567761546","feed_url":"https://feeds.transistor.fm/tinydevops",
		"google_podcasts":"https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL3RpbnlkZXZvcHM=","iHeartRadio":"",
		"overcast":"https://overcast.fm/itunes1567761546","pandora":"",
		"pocket_casts":"https://pca.st/itunes/1567761546","radioPublic":"https://radiopublic.com/tiny-devops-6BZx5R",
		"soundcloud":"","stitcher":"https://www.stitcher.com/podcast/tiny-devops",
		"tuneIn":"https://tunein.com/podcasts/Technology-Podcasts/Tiny-DevOps-p1437848/",
		"spotify":"https://open.spotify.com/show/2BEDe8fGJ8dn5LEk18MKDW",
		"apple_podcasts":"https://podcasts.apple.com/us/podcast/tiny-devops/id1567761546",
		"deezer":"https://www.deezer.com/show/2641062",
		"amazon_music":"https://music.amazon.com/podcasts/e3228372-fa03-48f9-969f-52734df5623c",
		"player_FM":"https://player.fm/series/tiny-devops",
		"podcast_addict":"https://podcastaddict.com/podcast/3337859","email_notifications":false},
		"relationships":{"episodes":{"data":[{"id":"1054248","type":"episode"},{"id":"1037180",
		"type":"episode"},{"id":"1020224","type":"episode"},{"id":"1020917","type":"episode"},
		{"id":"878294","type":"episode"},{"id":"871212","type":"episode"},{"id":"865774",
		"type":"episode"},{"id":"844846","type":"episode"},{"id":"843612","type":"episode"},
		{"id":"838643","type":"episode"},{"id":"830226","type":"episode"},{"id":"823070",
		"type":"episode"},{"id":"814614","type":"episode"},{"id":"811667","type":"episode"},
		{"id":"799800","type":"episode"},{"id":"793608","type":"episode"},{"id":"785434",
		"type":"episode"},{"id":"779293","type":"episode"},{"id":"777970","type":"episode"},
		{"id":"767666","type":"episode"},{"id":"764016","type":"episode"},{"id":"756382",
		"type":"episode"},{"id":"745347","type":"episode"},{"id":"740745","type":"episode"},
		{"id":"734040","type":"episode"},{"id":"727059","type":"episode"},{"id":"722080",
		"type":"episode"},{"id":"721148","type":"episode"},{"id":"710851","type":"episode"},
		{"id":"705092","type":"episode"},{"id":"710849","type":"episode"},{"id":"676283",
		"type":"episode"},{"id":"621509","type":"episode"},{"id":"615528","type":"episode"},
		{"id":"609064","type":"episode"},{"id":"602861","type":"episode"},{"id":"597141","type":"episode"},{"id":"591331","type":"episode"},{"id":"586286","type":"episode"},
		{"id":"578309","type":"episode"},{"id":"574094","type":"episode"},{"id":"568388",
		"type":"episode"},{"id":"560106","type":"episode"},{"id":"548717","type":"episode"},
		{"id":"532348","type":"episode"},{"id":"532211","type":"episode"},{"id":"531539",
		"type":"episode"},{"id":"542498","type":"episode"}]}}}}`),
		id: "20739",
		want: &Show{
			ID:                "20739",
			AmazonMusicURL:    "https://music.amazon.com/podcasts/e3228372-fa03-48f9-969f-52734df5623c",
			ApplePodcastsURL:  "https://podcasts.apple.com/us/podcast/tiny-devops/id1567761546",
			Author:            "Jonathan Hall",
			CastboxURL:        "https://castbox.fm/vic/1567761546",
			CastroURL:         "https://castro.fm/itunes/1567761546",
			Category:          "Technology",
			Copyright:         "© 2022 Jonathan Hall",
			CreatedAt:         parseTime("2021-04-28T18:54:18.263Z"),
			DeezerURL:         "https://www.deezer.com/show/2641062",
			Description:       "Solving big problems with small teams",
			FeedURL:           "https://feeds.transistor.fm/tinydevops",
			GooglePodcastsURL: "https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL3RpbnlkZXZvcHM=",
			ImageURL:          "https://images.transistor.fm/file/transistor/images/show/20739/1619720401-artwork.jpg",
			Keywords:          "devops,software development,programming,continuous delivery,continuous integration,cicd",
			Language:          "en",
			OvercastURL:       "https://overcast.fm/itunes1567761546",
			OwnerEmail:        "jonathan@jhall.io",
			PlayerFMURL:       "https://player.fm/series/tiny-devops",
			PlaylistLimit:     25,
			RadioPublicURL:    "https://radiopublic.com/tiny-devops-6BZx5R",
			SecondaryCategory: "Education :: How To",
			Slug:              "tinydevops",
			ShowType:          "episodic",
			SpotifyURL:        "https://open.spotify.com/show/2BEDe8fGJ8dn5LEk18MKDW",
			StitcherURL:       "https://www.stitcher.com/podcast/tiny-devops",
			TimeZone:          "Amsterdam",
			Title:             "Tiny DevOps",
			TuneInURL:         "https://tunein.com/podcasts/Technology-Podcasts/Tiny-DevOps-p1437848/",
			UpdatedAt:         parseTime("2022-10-11T09:55:00.804Z"),
			WebsiteURL:        "https://jhall.io/",
			PocketCastsURL:    "https://pca.st/itunes/1567761546",
			Episodes: []string{
				"1054248", "1037180", "1020224", "1020917", "878294",
				"871212", "865774", "844846", "843612", "838643", "830226",
				"823070", "814614", "811667", "799800", "793608", "785434",
				"779293", "777970", "767666", "764016", "756382", "745347",
				"740745", "734040", "727059", "722080", "721148", "710851",
				"705092", "710849", "676283", "621509", "615528", "609064",
				"602861", "597141", "591331", "586286", "578309", "574094",
				"568388", "560106", "548717", "532348", "532211", "531539",
				"542498",
			},
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, err := c.Show(context.Background(), tt.id)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}

func TestUpdateShow(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder testy.HTTPResponder
		show      *Show
		err       string
		status    int
		want      *Show
	}

	tests := testy.NewTable()
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusOK, `{"data":{"id":"35376","type":"show","attributes":{"author":"Jonathan Hall",
		"category":"","copyright":"","created_at":"2022-10-12T09:44:01.740Z","description":"Testing only","explicit":false,
		"image_url":null,"keywords":"","language":"en","multiple_seasons":false,"owner_email":"jonathan+apitest@jhall.io",
		"playlist_limit":25,"private":false,"secondary_category":"","show_type":"episodic","slug":"the-testcast",
		"time_zone":"Pacific Time (US \u0026 Canada)","title":"The Testcast","updated_at":"2022-10-12T10:07:26.250Z","website":"",
		"password_protected_feed":false,"castbox":null,"castro":null,"feed_url":"https://feeds.transistor.fm/the-testcast",
		"google_podcasts":null,"iHeartRadio":null,"overcast":null,"pandora":null,"pocket_casts":null,"radioPublic":null,
		"soundcloud":null,"stitcher":null,"tuneIn":null,"spotify":null,"apple_podcasts":null,"deezer":null,"amazon_music":null,
		"player_FM":null,"podcast_addict":null,"email_notifications":false},"relationships":{"episodes":{"data":[]}}}}`),
		show: &Show{
			ID:    testShowID,
			Title: "The Testcast",
		},
		want: &Show{
			ID:            testShowID,
			Author:        "Jonathan Hall",
			OwnerEmail:    "jonathan+apitest@jhall.io",
			FeedURL:       "https://feeds.transistor.fm/the-testcast",
			Description:   "Testing only",
			Language:      "en",
			PlaylistLimit: 25,
			Slug:          "the-testcast",
			ShowType:      "episodic",
			Title:         "The Testcast",
			TimeZone:      "Pacific Time (US & Canada)",
			Episodes:      []string{},
			CreatedAt:     parseTime("2022-10-12T09:44:01.740Z"),
			UpdatedAt:     parseTime("2022-10-12T10:07:26.250Z"),
		},
	})
	tests.Add("invalid category", tt{
		responder: newResponder(t, http.StatusUnprocessableEntity,
			`{"errors":[{"title":"show[category] does not have a valid value"}]}`),
		show: &Show{
			ID:    testShowID,
			Title: "The Testcast",
		},
		err:    "show[category] does not have a valid value",
		status: http.StatusUnprocessableEntity,
	})
	tests.Add("show not found", tt{
		responder: newResponder(t, http.StatusNotFound,
			`{"errors":[{"title":"Resource not found"}]}
			`),
		show: &Show{
			ID:    testShowID,
			Title: "The Testcast",
		},
		err:    "Resource not found",
		status: http.StatusNotFound,
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, err := c.UpdateShow(context.Background(), tt.show)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}
