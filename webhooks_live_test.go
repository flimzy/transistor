package transistor

import (
	"context"
	"testing"
)

func TestWebhooks_live(t *testing.T) {
	t.Parallel()
	c := liveClient(t)
	webhooks, err := c.Webhooks(context.Background(), testShowID)
	if err != nil {
		t.Fatal(err)
	}
	if len(webhooks) > 2 {
		t.Errorf("Wanted 2 webhooks, got %d", len(webhooks))
	}
}

func Test_Subscribe_and_Unsubscribe_live(t *testing.T) {
	c := liveClient(t)
	var webhook *Webhook
	t.Run("subscribe", func(t *testing.T) {
		var err error
		webhook, err = c.SubscribeWebhook(context.Background(), testShowID, "episode_created", "http://example.com/")
		if err != nil {
			t.Fatal(err)
		}
		if webhook.ShowID != testShowID {
			t.Errorf("Unexpected show ID returned: %s", webhook.ShowID)
		}
		t.Run("unsubscribe", func(t *testing.T) {
			webhook2, err := c.UnsubscribeWebhook(context.Background(), webhook.ID)
			if err != nil {
				t.Fatal(err)
			}
			if webhook2.ID != webhook.ID {
				t.Errorf("Unexpected ID returned: %s", webhook2.ID)
			}
		})
	})
}
