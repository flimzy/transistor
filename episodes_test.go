package transistor

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/flimzy/ale/errors"
	"gitlab.com/flimzy/testy"
)

func TestEpisodes(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder testy.HTTPResponder
		query     *EpisodesQuery
		err       string
		status    int
		want      []*Episode
		wantMeta  *Meta
	}

	tests := testy.NewTable()
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusOK, `{"data":[{"id":"1061187","type":"episode",
		"attributes":{"title":"Pilot","number":1,"season":1,"status":"draft","published_at":"2022-10-12T11:04:05.514Z",
		"duration":null,"explicit":false,"keywords":"","alternate_url":"",
		"media_url":"https://media.transistor.fm/57504521/863c58f7.mp3","image_url":null,"author":"","summary":"",
		"description":"","created_at":"2022-10-12T11:04:05.516Z","updated_at":"2022-10-12T11:04:05.516Z",
		"formatted_published_at":"October 12, 2022","duration_in_mmss":"00:00",
		"share_url":"https://share.transistor.fm/s/57504521","formatted_summary":"",
		"embed_html":"\u003ciframe width=\"100%\" height=\"180\" frameborder=\"no\" scrolling=\"no\" seamless src=\"https://share.transistor.fm/e/57504521\"\u003e\u003c/iframe\u003e",
		"embed_html_dark":"\u003ciframe width=\"100%\" height=\"180\" frameborder=\"no\" scrolling=\"no\" seamless src=\"https://share.transistor.fm/e/57504521/dark\"\u003e\u003c/iframe\u003e",
		"audio_processing":false,"type":"full","email_notifications":null},"relationships":{"show":{"data":{"id":"35376",
		"type":"show"}}}}],"meta":{"currentPage":1,"totalPages":1,"totalCount":1}}`),
		want: []*Episode{
			{
				ID:                   "1061187",
				Title:                "Pilot",
				ShowID:               "35376",
				CreatedAt:            parseTime("2022-10-12T11:04:05.516Z"),
				PublishedAt:          parseTime("2022-10-12T11:04:05.514Z"),
				UpdatedAt:            parseTime("2022-10-12T11:04:05.516Z"),
				EmbedHTML:            `<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/57504521"></iframe>`,
				FormattedDuration:    "00:00",
				EmbedHTMLDark:        `<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/57504521/dark"></iframe>`,
				FormattedPublishedAt: "October 12, 2022",
				MediaURL:             "https://media.transistor.fm/57504521/863c58f7.mp3",
				Season:               1,
				Number:               1,
				ShareURL:             "https://share.transistor.fm/s/57504521",
				Status:               "draft",
				Type:                 "full",
			},
		},
		wantMeta: &Meta{
			CurrentPage: 1,
			TotalPages:  1,
			TotalCount:  1,
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, pag, err := c.Episodes(context.Background(), tt.query)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
		if d := cmp.Diff(tt.wantMeta, pag); d != "" {
			t.Errorf("Pagination: %s\n", d)
		}
	})
}

func TestEpisode(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder testy.HTTPResponder
		id        string
		err       string
		status    int
		want      *Episode
	}

	tests := testy.NewTable()
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusOK, `{"data":{"id":"1061187","type":"episode","attributes":{"title":"Pilot",
		"number":1,"season":1,"status":"draft","published_at":"2022-10-12T11:04:05.514Z","duration":null,"explicit":false,
		"keywords":"","alternate_url":"","media_url":"https://media.transistor.fm/57504521/863c58f7.mp3","image_url":null,
		"author":"","summary":"","description":"","created_at":"2022-10-12T11:04:05.516Z","updated_at":"2022-10-12T11:04:05.516Z",
		"formatted_published_at":"October 12, 2022","duration_in_mmss":"00:00",
		"share_url":"https://share.transistor.fm/s/57504521","formatted_summary":"",
		"embed_html":"\u003ciframe width=\"100%\" height=\"180\" frameborder=\"no\" scrolling=\"no\" seamless src=\"https://share.transistor.fm/e/57504521\"\u003e\u003c/iframe\u003e",
		"embed_html_dark":"\u003ciframe width=\"100%\" height=\"180\" frameborder=\"no\" scrolling=\"no\" seamless src=\"https://share.transistor.fm/e/57504521/dark\"\u003e\u003c/iframe\u003e",
		"audio_processing":false,"type":"full","email_notifications":null},"relationships":{"show":{"data":{"id":"35376","type":"show"}}}}}`),
		id: "20739",
		want: &Episode{
			ID:                   "1061187",
			Title:                "Pilot",
			ShowID:               "35376",
			CreatedAt:            parseTime("2022-10-12T11:04:05.516Z"),
			PublishedAt:          parseTime("2022-10-12T11:04:05.514Z"),
			UpdatedAt:            parseTime("2022-10-12T11:04:05.516Z"),
			EmbedHTML:            `<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/57504521"></iframe>`,
			FormattedDuration:    "00:00",
			EmbedHTMLDark:        `<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/57504521/dark"></iframe>`,
			FormattedPublishedAt: "October 12, 2022",
			MediaURL:             "https://media.transistor.fm/57504521/863c58f7.mp3",
			Season:               1,
			Number:               1,
			ShareURL:             "https://share.transistor.fm/s/57504521",
			Status:               "draft",
			Type:                 "full",
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, err := c.Episode(context.Background(), tt.id)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}

func TestAuthorizeUpload(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder testy.HTTPResponder
		filename  string
		err       string
		status    int
		want      *AudioUpload
	}

	tests := testy.NewTable()
	tests.Add("unsupported type", tt{
		responder: newResponder(t, http.StatusBadRequest, `{"errors":[{"title":"filename must be one of: '.mp3, .m4a, .wav, .aif, .aiff, .aifc, .mp4, or .mov' types"}]}`),
		filename:  "foo.txt",
		err:       "filename must be one of: '.mp3, .m4a, .wav, .aif, .aiff, .aifc, .mp4, or .mov' types",
		status:    http.StatusBadRequest,
	})
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusOK, `{"data":{"id":"b7b4d8b1-c85e-4fce-ba60-0d8f9c279044","type":"audio_upload","attributes":{"upload_url":"https://transistorupload.s3.amazonaws.com/uploads/api/b7b4d8b1-c85e-4fce-ba60-0d8f9c279044/foo.mp3?x-amz-acl=public-read\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=AKIAJNPHH33F4RA7EJFA%2F20221012%2Fus-east-1%2Fs3%2Faws4_request\u0026X-Amz-Date=20221012T115358Z\u0026X-Amz-Expires=600\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=47267a0f442cbe39d158145b4f0005ff9f88687d91865c78c3610952cf569ade","content_type":"audio/mpeg","expires_in":600,"audio_url":"https://transistorupload.s3.amazonaws.com/uploads/api/b7b4d8b1-c85e-4fce-ba60-0d8f9c279044/foo.mp3"}}}`), //nolint:revive // can't reasonably split this line
		filename:  "foo.txt",
		want: &AudioUpload{
			ID:          "b7b4d8b1-c85e-4fce-ba60-0d8f9c279044",
			ContentType: "audio/mpeg",
			UploadURL:   "https://transistorupload.s3.amazonaws.com/uploads/api/b7b4d8b1-c85e-4fce-ba60-0d8f9c279044/foo.mp3?x-amz-acl=public-read&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJNPHH33F4RA7EJFA%2F20221012%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20221012T115358Z&X-Amz-Expires=600&X-Amz-SignedHeaders=host&X-Amz-Signature=47267a0f442cbe39d158145b4f0005ff9f88687d91865c78c3610952cf569ade", //nolint:revive // can't reasonably split this line
			ExpiresIn:   600,
			AudioURL:    "https://transistorupload.s3.amazonaws.com/uploads/api/b7b4d8b1-c85e-4fce-ba60-0d8f9c279044/foo.mp3",
		},
	})
	tests.Add("missing filename", tt{
		responder: newResponder(t, http.StatusInternalServerError, `{"errors":[{"title":"Unknown server error"}]}`),
		filename:  "",
		err:       "Unknown server error",
		status:    http.StatusInternalServerError,
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, err := c.AuthorizeUpload(context.Background(), tt.filename)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}

func TestCreateEpisode(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder testy.HTTPResponder
		episode   *Episode
		err       string
		status    int
		want      *Episode
	}

	tests := testy.NewTable()
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusCreated, `{"data":{"id":"1061344","type":"episode","attributes":
		{"title":"new test episode","number":null,"season":1,"status":"draft","published_at":null,"duration":null,
		"explicit":false,"keywords":null,"alternate_url":null,"media_url":"https://media.transistor.fm/2a8eb796/f59424b4.mp3",
		"image_url":null,"author":null,"summary":null,"description":null,"created_at":"2022-10-12T12:56:42.304Z",
		"updated_at":"2022-10-12T12:56:42.304Z","formatted_published_at":null,"duration_in_mmss":"00:00",
		"share_url":"https://share.transistor.fm/s/2a8eb796","formatted_summary":null,
		"embed_html":"\u003ciframe width=\"100%\" height=\"180\" frameborder=\"no\" scrolling=\"no\" seamless src=\"https://share.transistor.fm/e/2a8eb796\"\u003e\u003c/iframe\u003e",
		"embed_html_dark":"\u003ciframe width=\"100%\" height=\"180\" frameborder=\"no\" scrolling=\"no\" seamless src=\"https://share.transistor.fm/e/2a8eb796/dark\"\u003e\u003c/iframe\u003e",
		"audio_processing":false,"type":"full","email_notifications":null},"relationships":{"show":{"data":{"id":"35376","type":"show"}}}}}`),
		episode: &Episode{
			ShowID: "35376",
			Title:  "new test episode",
		},
		want: &Episode{
			ID:                "1061344",
			CreatedAt:         parseTime("2022-10-12T12:56:42.304Z"),
			UpdatedAt:         parseTime("2022-10-12T12:56:42.304Z"),
			FormattedDuration: "00:00",
			EmbedHTML:         `<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/2a8eb796"></iframe>`,
			EmbedHTMLDark:     `<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/2a8eb796/dark"></iframe>`,
			MediaURL:          "https://media.transistor.fm/2a8eb796/f59424b4.mp3",
			Season:            1,
			ShareURL:          "https://share.transistor.fm/s/2a8eb796",
			Status:            "draft",
			Title:             "new test episode",
			Type:              "full",
			ShowID:            "35376",
		},
	})
	tests.Add("with transcript", tt{
		responder: newResponder(t, http.StatusCreated, `{
			"data": {
			  "id": "1094096",
			  "type": "episode",
			  "attributes": {
				"title": "new test episode",
				"number": null,
				"season": 1,
				"status": "draft",
				"published_at": null,
				"duration": null,
				"explicit": false,
				"keywords": null,
				"alternate_url": null,
				"transcript_url": "https://share.transistor.fm/s/8356c2c7/transcript.txt",
				"media_url": "https://media.transistor.fm/8356c2c7/173bb136.mp3",
				"image_url": null,
				"author": null,
				"summary": null,
				"description": null,
				"created_at": "2022-11-11T09:08:32.894Z",
				"updated_at": "2022-11-11T09:08:32.906Z",
				"formatted_published_at": null,
				"duration_in_mmss": "00:00",
				"share_url": "https://share.transistor.fm/s/8356c2c7",
				"formatted_summary": null,
				"embed_html": "<iframe width=\"100%\" height=\"180\" frameborder=\"no\" scrolling=\"no\" seamless src=\"https://share.transistor.fm/e/8356c2c7\"></iframe>",
				"embed_html_dark": "<iframe width=\"100%\" height=\"180\" frameborder=\"no\" scrolling=\"no\" seamless src=\"https://share.transistor.fm/e/8356c2c7/dark\"></iframe>",
				"audio_processing": false,
				"type": "full",
				"email_notifications": null
			  },
			  "relationships": {
				"show": {
				  "data": {
					"id": "35376",
					"type": "show"
				  }
				}
			  }
			}
		  }
		`),
		episode: &Episode{
			ShowID:         testShowID,
			Title:          "new test episode",
			TranscriptText: "This is a test transcript",
		},
		want: &Episode{
			ID:                "1094096",
			TranscriptURL:     "https://share.transistor.fm/s/8356c2c7/transcript.txt",
			CreatedAt:         parseTime("2022-11-11T09:08:32.894Z"),
			UpdatedAt:         parseTime("2022-11-11T09:08:32.906Z"),
			FormattedDuration: "00:00",
			EmbedHTML:         `<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/8356c2c7"></iframe>`,
			EmbedHTMLDark:     `<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/8356c2c7/dark"></iframe>`,
			MediaURL:          "https://media.transistor.fm/8356c2c7/173bb136.mp3",
			Season:            1,
			ShareURL:          "https://share.transistor.fm/s/8356c2c7",
			Status:            "draft",
			Title:             "new test episode",
			Type:              "full",
			ShowID:            "35376",
		},
	})
	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, err := c.CreateEpisode(context.Background(), tt.episode)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}

func TestUpdateEpisode(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder testy.HTTPResponder
		episode   *Episode
		err       string
		status    int
		want      *Episode
	}

	tests := testy.NewTable()
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusOK, `{"data":{"id":"1061187","type":"episode","attributes":{"title":"Pilot",
		"number":1,"season":1,"status":"draft","published_at":"2022-10-12T11:04:05.514Z","duration":null,"explicit":false,
		"keywords":"","alternate_url":"","media_url":"https://media.transistor.fm/57504521/863c58f7.mp3","image_url":null,
		"author":"","summary":"","description":"","created_at":"2022-10-12T11:04:05.516Z","updated_at":"2022-10-12T11:04:05.516Z",
		"formatted_published_at":"October 12, 2022","duration_in_mmss":"00:00",
		"share_url":"https://share.transistor.fm/s/57504521","formatted_summary":"",
		"embed_html":"\u003ciframe width=\"100%\" height=\"180\" frameborder=\"no\" scrolling=\"no\" seamless src=\"https://share.transistor.fm/e/57504521\"\u003e\u003c/iframe\u003e",
		"embed_html_dark":"\u003ciframe width=\"100%\" height=\"180\" frameborder=\"no\" scrolling=\"no\" seamless src=\"https://share.transistor.fm/e/57504521/dark\"\u003e\u003c/iframe\u003e",
		"audio_processing":false,"type":"full","email_notifications":null},"relationships":{"show":{"data":{"id":"35376",
		"type":"show"}}}}}`),
		episode: &Episode{
			ID:    testEpisodeID,
			Title: "The Testcast",
		},
		want: &Episode{
			ID:                   "1061187",
			CreatedAt:            parseTime("2022-10-12T11:04:05.516Z"),
			UpdatedAt:            parseTime("2022-10-12T11:04:05.516Z"),
			FormattedDuration:    "00:00",
			EmbedHTML:            `<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/57504521"></iframe>`,
			EmbedHTMLDark:        `<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/57504521/dark"></iframe>`,
			FormattedPublishedAt: "October 12, 2022",
			MediaURL:             "https://media.transistor.fm/57504521/863c58f7.mp3",
			Number:               1,
			PublishedAt:          parseTime("2022-10-12T11:04:05.514Z"),
			Season:               1,
			ShareURL:             "https://share.transistor.fm/s/57504521",
			Title:                "Pilot",
			ShowID:               "35376",
			Type:                 "full",
			Status:               "draft",
		},
	})
	tests.Add("episode not found", tt{
		responder: newResponder(t, http.StatusNotFound, `{"errors":[{"title":"Resource not found"}]}`),
		episode: &Episode{
			ID:    "oink",
			Title: "Pilot",
		},
		err:    "Resource not found",
		status: http.StatusNotFound,
	})
	tests.Add("invalid type", tt{
		responder: newResponder(t, http.StatusUnprocessableEntity,
			`{"errors":[{"title":"episode[type] does not have a valid value"}]}`),
		episode: &Episode{
			ID:    testEpisodeID,
			Title: "Pilot",
			Type:  "oink",
		},
		err:    "episode[type] does not have a valid value",
		status: http.StatusUnprocessableEntity,
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, err := c.UpdateEpisode(context.Background(), tt.episode)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}

func TestPublishEpisode(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder    testy.HTTPResponder
		id, epStatus string
		publishAt    time.Time
		err          string
		status       int
		want         *Episode
	}

	tests := testy.NewTable()
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusOK, `{"data":{"id":"1061187","type":"episode","attributes":{"title":"Pilot",
		"number":1,"season":1,"status":"draft","published_at":null,"duration":null,"explicit":false,"keywords":"",
		"alternate_url":"","media_url":"https://media.transistor.fm/57504521/863c58f7.mp3","image_url":null,"author":"",
		"summary":"","description":"","created_at":"2022-10-12T11:04:05.516Z","updated_at":"2022-10-12T13:39:59.372Z",
		"formatted_published_at":null,"duration_in_mmss":"00:00","share_url":"https://share.transistor.fm/s/57504521","formatted_summary":"",
		"embed_html":"\u003ciframe width=\"100%\" height=\"180\" frameborder=\"no\" scrolling=\"no\" seamless src=\"https://share.transistor.fm/e/57504521\"\u003e\u003c/iframe\u003e",
		"embed_html_dark":"\u003ciframe width=\"100%\" height=\"180\" frameborder=\"no\" scrolling=\"no\" seamless src=\"https://share.transistor.fm/e/57504521/dark\"\u003e\u003c/iframe\u003e",
		"audio_processing":false,"type":"full","email_notifications":null},"relationships":{"show":{"data":{"id":"35376","type":"show"}}}}}`),
		id:       testEpisodeID,
		epStatus: "draft",
		want: &Episode{
			ID:                "1061187",
			CreatedAt:         parseTime("2022-10-12T11:04:05.516Z"),
			UpdatedAt:         parseTime("2022-10-12T13:39:59.372Z"),
			FormattedDuration: "00:00",
			EmbedHTML:         `<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/57504521"></iframe>`,
			EmbedHTMLDark:     `<iframe width="100%" height="180" frameborder="no" scrolling="no" seamless src="https://share.transistor.fm/e/57504521/dark"></iframe>`,
			MediaURL:          "https://media.transistor.fm/57504521/863c58f7.mp3",
			Number:            1,
			Season:            1,
			ShareURL:          "https://share.transistor.fm/s/57504521",
			Title:             "Pilot",
			ShowID:            "35376",
			Type:              "full",
			Status:            "draft",
		},
	})
	tests.Add("invalid status", tt{
		responder: newResponder(t, http.StatusUnprocessableEntity,
			`{"errors":[{"title":"episode[status] does not have a valid value"}]}`),
		id:       testEpisodeID,
		epStatus: "draftx",
		err:      "episode[status] does not have a valid value",
		status:   http.StatusUnprocessableEntity,
	})
	tests.Add("can't publish in the future", tt{
		responder: newResponder(t, http.StatusUnprocessableEntity,
			`{"errors":[{"title":"You must specify status as 'scheduled' if providing a future date."}]}`),
		id:        testEpisodeID,
		epStatus:  "published",
		publishAt: parseTime("3000-01-01T01:01:01Z"),
		err:       "You must specify status as 'scheduled' if providing a future date.",
		status:    http.StatusUnprocessableEntity,
	})
	tests.Add("can't schedule in the past", tt{
		responder: newResponder(t, http.StatusUnprocessableEntity,
			`{"errors":[{"title":"You must provide a future date if specifying 'scheduled' status."}]}`),
		id:        testEpisodeID,
		epStatus:  "scheduled",
		publishAt: parseTime("1000-01-01T01:01:01Z"),
		err:       "You must provide a future date if specifying 'scheduled' status.",
		status:    http.StatusUnprocessableEntity,
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, err := c.PublishEpisode(context.Background(), tt.id, tt.epStatus, tt.publishAt)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}
