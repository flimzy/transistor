package transistor

import (
	"encoding/json"
	"strconv"
	"time"
)

// The Transistor.fm API uses two contradicting and ambiguous date formats...
// often in the same response! 🤦
const (
	dateFormatDMY = "02-01-2006"
	dateFormatMDY = "01-02-2006"
)

// dateDMY is a dateDMY in DD-MM-YYYY format, as used by the Transistor.fm API.
type dateDMY time.Time

func (d *dateDMY) UnmarshalText(p []byte) error {
	t, err := time.Parse(dateFormatDMY, string(p))
	*d = dateDMY(t)
	return err
}

// dateDMY is a dateDMY in MM-DD-YYYY format, as also used by the Transistor.fm
// API.
type dateMDY time.Time

func (d *dateMDY) UnmarshalText(p []byte) error {
	t, err := time.Parse(dateFormatMDY, string(p))
	*d = dateMDY(t)
	return err
}

type stringOrInt string

func (s *stringOrInt) UnmarshalJSON(p []byte) error {
	if p[0] == '"' {
		var str string
		if err := json.Unmarshal(p, &str); err != nil {
			return err
		}
		*s = stringOrInt(str)
		return nil
	}
	var i int
	if err := json.Unmarshal(p, &i); err != nil {
		return err
	}
	*s = stringOrInt(strconv.Itoa(i))
	return nil
}
