package transistor

import (
	"context"
	"os"
	"testing"
)

func TestSubscribers_live(t *testing.T) {
	t.Skip()
	apiKey := os.Getenv("TRANSISTOR_API_KEY")
	if apiKey == "" {
		t.Skip("TRANSISTOR_API_KEY not set, skipping live test")
	}
	c, err := New("", apiKey)
	c.debug = true
	if err != nil {
		t.Fatal(err)
	}
	subscribers, _, err := c.Subscribers(context.Background(), &SubscribersQuery{
		ShowID: testShowID,
	})
	if err != nil {
		t.Fatal(err)
	}
	if len(subscribers) > 0 {
		t.Errorf("Expected 0 results, got %d", len(subscribers))
	}
}
