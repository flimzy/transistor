[![Go Report Card](https://goreportcard.com/badge/gitlab.com/flimzy/transistor)](https://goreportcard.com/report/gitlab.com/flimzy/transistor)
[![GoDoc](https://godoc.org/gitlab.com/flimzy/transistor?status.svg)](https://pkg.go.dev/gitlab.com/flimzy/transistor)

# Transistor.fm Go SDK

Done:

- GET /v1 - [User()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.User)
- GET /v1/shows - [Shows()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.Shows)
- GET /v1/shows/:id - [Show()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.Show)
- PATCH /v1/shows/:id - [UpdateShow()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.UpdateShow)
- GET /v1/episodes - [Episodes()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.Episodes)
- GET /v1/episodes/:id - [Episode()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.Episode)
- GET /v1/episodes/authorize_upload - [AuthorizeUpload()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.AuthorizeUpload)
- POST /v1/episodes - [CreateEpisode()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.CreateEpisode)
- PATCH /v1/episodes/:id - [UpdateEpisode()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.UpdateEpisode)
- PATCH /v1/episodes/:id/publish - [PublishEpisode()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.PublishEpisode)
- GET /v1/webhooks - [Webhooks()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.Webhooks)
- POST /v1/webhooks - [SubscribeWebhook()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.SubscribeWebhook)
- DELETE /v1/webhooks/:id - [UnsubscribeWebhook()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.UnsubscribeWebhook)
- GET /v1/analytics/:id - [ShowAnalytics()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.ShowAnalytics)
- GET /v1/analytics/:id/episodes - [AllEpisodesAnalytics()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.AllEpisodesAnalytics)
- GET /v1/analytics/episodes/:id - [EpisodeAnalytics()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.EpisodeAnalytics)

Needs tests:

- GET /v1/subscribers - [Subscribers()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.Subscribers)
- GET /v1/subscribers/:id - [Subscriber()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.Subscriber)
- POST /v1/subscribers - [CreateSubscriber()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.CreateSubscriber)
- POST /v1/subscribers/batch - [CreateSubscribers()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.CreateSubscribers)
- PATCH /v1/subscribers/:id - [UpdateSubscriber()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.UpdateSubscriber)
- DELETE /v1/subscribers - [DeleteSubscriberEmail()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.DeleteSubscriberEmail)
- DELETE /v1/subscribers/:id - [DeleteSubscriber()](https://pkg.go.dev/gitlab.com/flimzy/transistor#Client.DeleteSubscriber)
