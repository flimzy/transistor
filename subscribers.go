package transistor

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"time"
)

// SubscribersQuery is a query for subscribers.
type SubscribersQuery struct {
	ShowID     string `url:"show_id"`
	Pagination `url:"pagination"`
	Fields     map[string][]string `url:"fields"`
}

// Subscriber is an individual subscriber record for a private podcast.
//
// See https://developers.transistor.fm/#Subscriber
type Subscriber struct {
	ID               string    `json:"id" url:"-"`
	CreatedAt        time.Time `json:"created_at" url:"-"`
	Email            string    `json:"email" url:"email"`
	FeedURL          string    `json:"feed_url" url:"-"`
	HasDownloads     bool      `json:"has_downloads" url:"-"`
	LastNotifiedAt   time.Time `json:"last_notified_at" url:"-"`
	Status           string    `json:"status" url:"-"`
	SubscribeURL     string    `json:"subscribe_url" url:"-"`
	UpdatedAt        time.Time `json:"updated_at" url:"-"`
	ShowID           string    `json:"-" url:"show_id,omitempty"`
	SkipWelcomeEmail string    `json:"-" url:"skip_welcome_email,omitempty"`
}

// UnmarshalJSON satisfies the json.Unmarshaler interface.
func (s *Subscriber) UnmarshalJSON(p []byte) error {
	target := struct {
		*Subscriber
		Attributes    json.RawMessage `json:"attributes"`
		Relationships json.RawMessage `json:"relationships"`
		UnmarshalJSON struct{}        `json:"-"`
	}{
		Subscriber: s,
	}
	if err := json.Unmarshal(p, &target); err != nil {
		return err
	}
	if len(target.Attributes) > 0 {
		if err := json.Unmarshal(target.Attributes, &target); err != nil {
			return err
		}
	}
	if len(target.Relationships) > 0 {
		rels := new(relationships)
		if err := json.Unmarshal(target.Relationships, &rels); err != nil {
			return err
		}
		s.ShowID = rels.get("show")[0]
	}
	return nil
}

// Subscribers retrieves a list of all subscribers for a single private podcast.
//
// See https://developers.transistor.fm/#subscribers
func (c *Client) Subscribers(ctx context.Context, query *SubscribersQuery) ([]*Subscriber, *Meta, error) {
	response := new(struct {
		Data []*Subscriber `json:"data"`
		Meta *Meta         `json:"meta"`
	})
	err := c.doRequest(ctx, http.MethodGet, c.path("subscribers").query(query), nil, response)
	return response.Data, response.Meta, err
}

// Subscriber retrieves a single private podcast subscriber.
//
// See https://developers.transistor.fm/#subscribers
func (c *Client) Subscriber(ctx context.Context, subscriberID string) (*Subscriber, error) {
	response := new(struct {
		Data *Subscriber `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodGet, c.path("subscribers", subscriberID), nil, response)
	return response.Data, err
}

// CreateSubscriber adds a single subscriber to a private podcast, and send an
// optional instructional email.
//
// See https://developers.transistor.fm/#subscribers
func (c *Client) CreateSubscriber(ctx context.Context, subscriber *Subscriber) (*Subscriber, error) {
	if subscriber == nil {
		return nil, errors.New("subscriber required")
	}
	if subscriber.ShowID == "" {
		return nil, errors.New("subscriber.ShowID required")
	}
	if subscriber.Email == "" {
		return nil, errors.New("subscriber.Email required")
	}
	response := new(struct {
		Data *Subscriber `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodPost, c.path("subscribers"), subscriber, response)
	return response.Data, err
}

// CreateSubscribers adds a batch of multiple subscribers to a private podcast,
// and sends them optional instructional emails.
//
// See https://developers.transistor.fm/#subscribers
func (c *Client) CreateSubscribers(ctx context.Context, showID string, skipEmail bool, email ...string) ([]*Subscriber, error) {
	if showID == "" {
		return nil, errors.New("showID required")
	}
	if len(email) == 0 {
		return nil, errors.New("email required")
	}
	request := struct {
		ShowID    string   `url:"show_id"`
		Emails    []string `url:"emails"`
		SkipEmail bool     `url:"skip_welcome_email,omitempty"`
	}{
		ShowID:    showID,
		Emails:    email,
		SkipEmail: skipEmail,
	}
	response := new(struct {
		Data []*Subscriber `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodPost, c.path("subscribers/batch"), request, response)
	return response.Data, err
}

// UpdateSubscriber updates a single private podcast subscriber.
//
// See https://developers.transistor.fm/#subscribers
func (c *Client) UpdateSubscriber(ctx context.Context, subscriber *Subscriber) (*Subscriber, error) {
	if subscriber == nil {
		return nil, errors.New("subscriber required")
	}
	if subscriber.ID == "" {
		return nil, errors.New("subscriber.ID required")
	}
	if subscriber.Email == "" {
		return nil, errors.New("subscriber.Email required")
	}
	request := struct {
		Subscriber *Subscriber `url:"subscriber"`
	}{
		Subscriber: subscriber,
	}
	response := new(struct {
		Data *Subscriber `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodPatch, c.path("subscribers", subscriber.ID), request, response)
	return response.Data, err
}

// DeleteSubscriberEmail removes a single private podcast subscriber by email
// and revokes their access to the podcast.
//
// showID may be the show ID or slug.
//
// See https://developers.transistor.fm/#subscribers
func (c *Client) DeleteSubscriberEmail(ctx context.Context, showID, email string) (*Subscriber, error) {
	if showID == "" {
		return nil, errors.New("showID required")
	}
	if email == "" {
		return nil, errors.New("email required")
	}
	request := struct {
		ShowID string `id:"show_id"`
		Email  string `id:"email"`
	}{
		ShowID: showID,
		Email:  email,
	}
	response := new(struct {
		Data *Subscriber `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodDelete, c.path("subscribers"), request, response)
	return response.Data, err
}

// DeleteSubscriber removes a single private podcast subscriber and revokes
// their access to the podcast.
//
// See https://developers.transistor.fm/#subscribers
func (c *Client) DeleteSubscriber(ctx context.Context, subscriberID string) (*Subscriber, error) {
	response := new(struct {
		Data *Subscriber `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodDelete, c.path("subscribers", subscriberID), nil, response)
	return response.Data, err
}
