package transistor

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/flimzy/ale/errors"
	"gitlab.com/flimzy/testy"
)

func TestShowAnalytics(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder testy.HTTPResponder
		showID    string
		query     *AnalyticsQuery
		err       string
		status    int
		want      *ShowAnalytics
	}

	tests := testy.NewTable()
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusOK, `{"data":{"id":"the-testcast","type":"show_analytics","attributes":
		{"downloads":[{"date":"29-09-2022","downloads":0},{"date":"30-09-2022","downloads":0},{"date":"01-10-2022","downloads":0},
		{"date":"02-10-2022","downloads":0},{"date":"03-10-2022","downloads":0},{"date":"04-10-2022","downloads":0},
		{"date":"05-10-2022","downloads":0},{"date":"06-10-2022","downloads":0},{"date":"07-10-2022","downloads":0},
		{"date":"08-10-2022","downloads":0},{"date":"09-10-2022","downloads":0},{"date":"10-10-2022","downloads":0},
		{"date":"11-10-2022","downloads":0},{"date":"12-10-2022","downloads":0}],"start_date":"09-29-2022",
		"end_date":"10-12-2022"},"relationships":{"show":{"data":{"id":"35376","type":"show"}}}}}`),
		showID: testShowID,
		want: &ShowAnalytics{
			ID: "the-testcast",
			Downloads: []Downloads{
				{Date: parseTime("2022-09-29T00:00:00Z")},
				{Date: parseTime("2022-09-30T00:00:00Z")},
				{Date: parseTime("2022-10-01T00:00:00Z")},
				{Date: parseTime("2022-10-02T00:00:00Z")},
				{Date: parseTime("2022-10-03T00:00:00Z")},
				{Date: parseTime("2022-10-04T00:00:00Z")},
				{Date: parseTime("2022-10-05T00:00:00Z")},
				{Date: parseTime("2022-10-06T00:00:00Z")},
				{Date: parseTime("2022-10-07T00:00:00Z")},
				{Date: parseTime("2022-10-08T00:00:00Z")},
				{Date: parseTime("2022-10-09T00:00:00Z")},
				{Date: parseTime("2022-10-10T00:00:00Z")},
				{Date: parseTime("2022-10-11T00:00:00Z")},
				{Date: parseTime("2022-10-12T00:00:00Z")},
			},
			ShowID: testShowID,
		},
	})
	tests.Add("start date without end date", tt{
		responder: newResponder(t, http.StatusUnprocessableEntity,
			`{"errors":[{"title":"start_date provide all or none of parameters"},{"title":"end_date provide all or none of parameters"}]}`),
		showID: testShowID,
		query: &AnalyticsQuery{
			StartDate: time.Now(),
		},
		err:    "start_date provide all or none of parameters\nend_date provide all or none of parameters",
		status: http.StatusUnprocessableEntity,
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, err := c.ShowAnalytics(context.Background(), tt.showID, tt.query)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}

func TestAllEpisodesAnalytics(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder testy.HTTPResponder
		showID    string
		query     *AnalyticsQuery
		err       string
		status    int
		want      *EpisodesAnalytics
	}

	tests := testy.NewTable()
	tests.Add("success, no published episodes", tt{
		responder: newResponder(t, http.StatusOK, `{"data":{"id":"the-testcast","type":"episodes_analytics",
		"attributes":{"episodes":[],"start_date":"10-06-2022","end_date":"10-12-2022"},"relationships":{"show":{"data":{"id":"35376","type":"show"}}}}}`),
		showID: testShowID,
		want: &EpisodesAnalytics{
			ID:       "the-testcast",
			Episodes: []EpisodeAnalytics{},
			ShowID:   testShowID,
		},
	})
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusOK, `{"data":{"id":"the-testcast","type":"episodes_analytics",
		"attributes":{"episodes":[{"id":1061187,"title":"Pilot","published_at":"2022-10-12T08:13:48.125-07:00",
		"downloads":[{"date":"10-12-2022","downloads":0},{"date":"10-11-2022","downloads":0},{"date":"10-10-2022",
		"downloads":0},{"date":"10-09-2022","downloads":0},{"date":"10-08-2022","downloads":0},{"date":"10-07-2022",
		"downloads":0},{"date":"10-06-2022","downloads":0}]}],"start_date":"10-06-2022","end_date":"10-12-2022"},
		"relationships":{"show":{"data":{"id":"35376","type":"show"}}}}}`),
		showID: testShowID,
		want: &EpisodesAnalytics{
			ID: "the-testcast",
			Episodes: []EpisodeAnalytics{
				{
					ID: "1061187",
					Downloads: []Downloads{
						{Date: parseTime("2022-10-12T00:00:00Z")},
						{Date: parseTime("2022-10-11T00:00:00Z")},
						{Date: parseTime("2022-10-10T00:00:00Z")},
						{Date: parseTime("2022-10-09T00:00:00Z")},
						{Date: parseTime("2022-10-08T00:00:00Z")},
						{Date: parseTime("2022-10-07T00:00:00Z")},
						{Date: parseTime("2022-10-06T00:00:00Z")},
					},
				},
			},
			ShowID: testShowID,
		},
	})
	tests.Add("doc example", tt{
		responder: newResponder(t, http.StatusOK, `{
			"data": {
			  "id": "the-caffeine-show",
			  "type": "episodes_analytics",
			  "attributes": {
				"episodes": [
				  {
					"id": 2,
					"title": "Episode Two",
					"published_at": "2022-10-27 15:41:42 UTC",
					"downloads": [
					  {
						"date": "10-22-2022",
						"downloads": 0
					  },
					  {
						"date": "10-23-2022",
						"downloads": 0
					  },
					  {
						"date": "10-24-2022",
						"downloads": 0
					  },
					  {
						"date": "10-25-2022",
						"downloads": 0
					  },
					  {
						"date": "10-26-2022",
						"downloads": 0
					  },
					  {
						"date": "10-27-2022",
						"downloads": 0
					  },
					  {
						"date": "10-28-2022",
						"downloads": 0
					  }
					]
				  },
				  {
					"id": 1,
					"title": "Episode One",
					"published_at": "2022-10-25 15:41:42 UTC",
					"downloads": [
					  {
						"date": "10-22-2022",
						"downloads": 0
					  },
					  {
						"date": "10-23-2022",
						"downloads": 0
					  },
					  {
						"date": "10-24-2022",
						"downloads": 0
					  },
					  {
						"date": "10-25-2022",
						"downloads": 0
					  },
					  {
						"date": "10-26-2022",
						"downloads": 0
					  },
					  {
						"date": "10-27-2022",
						"downloads": 0
					  },
					  {
						"date": "10-28-2022",
						"downloads": 0
					  }
					]
				  }
				],
				"start_date": "10-22-2022",
				"end_date": "10-28-2022"
			  },
			  "relationships": {
				"show": {
				  "data": {
					"id": "132543",
					"type": "show"
				  }
				}
			  }
			},
			"included": [
			  {
				"id": "132543",
				"type": "show",
				"attributes": {
				  "title": "The Caffeine Show"
				},
				"relationships": {}
			  }
			]
		  }`),
		showID: testShowID,
		want: &EpisodesAnalytics{
			ID:     "the-caffeine-show",
			ShowID: "132543",
			Episodes: []EpisodeAnalytics{
				{
					ID: "2",
					Downloads: []Downloads{
						{Date: parseTime("2022-10-22T00:00:00Z")},
						{Date: parseTime("2022-10-23T00:00:00Z")},
						{Date: parseTime("2022-10-24T00:00:00Z")},
						{Date: parseTime("2022-10-25T00:00:00Z")},
						{Date: parseTime("2022-10-26T00:00:00Z")},
						{Date: parseTime("2022-10-27T00:00:00Z")},
						{Date: parseTime("2022-10-28T00:00:00Z")},
					},
				},
				{
					ID: "1",
					Downloads: []Downloads{
						{Date: parseTime("2022-10-22T00:00:00Z")},
						{Date: parseTime("2022-10-23T00:00:00Z")},
						{Date: parseTime("2022-10-24T00:00:00Z")},
						{Date: parseTime("2022-10-25T00:00:00Z")},
						{Date: parseTime("2022-10-26T00:00:00Z")},
						{Date: parseTime("2022-10-27T00:00:00Z")},
						{Date: parseTime("2022-10-28T00:00:00Z")},
					},
				},
			},
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, err := c.AllEpisodesAnalytics(context.Background(), tt.showID, tt.query)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}

func TestEpisodeAnalytics(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder testy.HTTPResponder
		episodeID string
		query     *AnalyticsQuery
		err       string
		status    int
		want      *EpisodeAnalytics
	}

	tests := testy.NewTable()

	tests.Add("doc example", tt{
		responder: newResponder(t, http.StatusOK, `{
			"data": {
			  "id": "3056098",
			  "type": "episode_analytics",
			  "attributes": {
				"downloads": [
				  {
					"date": "22-10-2022",
					"downloads": 0
				  },
				  {
					"date": "23-10-2022",
					"downloads": 0
				  },
				  {
					"date": "24-10-2022",
					"downloads": 0
				  },
				  {
					"date": "25-10-2022",
					"downloads": 0
				  },
				  {
					"date": "26-10-2022",
					"downloads": 0
				  },
				  {
					"date": "27-10-2022",
					"downloads": 0
				  },
				  {
					"date": "28-10-2022",
					"downloads": 0
				  },
				  {
					"date": "29-10-2022",
					"downloads": 0
				  },
				  {
					"date": "30-10-2022",
					"downloads": 0
				  },
				  {
					"date": "31-10-2022",
					"downloads": 0
				  },
				  {
					"date": "01-11-2022",
					"downloads": 0
				  },
				  {
					"date": "02-11-2022",
					"downloads": 0
				  },
				  {
					"date": "03-11-2022",
					"downloads": 0
				  },
				  {
					"date": "04-11-2022",
					"downloads": 0
				  }
				],
				"start_date": "10-22-2022",
				"end_date": "11-04-2022"
			  },
			  "relationships": {
				"episode": {
				  "data": {
					"id": "3056098",
					"type": "episode"
				  }
				}
			  }
			},
			"included": [
			  {
				"id": "3056098",
				"type": "episode",
				"attributes": {
				  "title": "How To Roast Coffee"
				},
				"relationships": {}
			  }
			]
		  }`),
		episodeID: testShowID,
		want: &EpisodeAnalytics{
			ID: "3056098",
			Downloads: []Downloads{
				{Date: parseTime("2022-10-22T00:00:00Z")},
				{Date: parseTime("2022-10-23T00:00:00Z")},
				{Date: parseTime("2022-10-24T00:00:00Z")},
				{Date: parseTime("2022-10-25T00:00:00Z")},
				{Date: parseTime("2022-10-26T00:00:00Z")},
				{Date: parseTime("2022-10-27T00:00:00Z")},
				{Date: parseTime("2022-10-28T00:00:00Z")},
				{Date: parseTime("2022-10-29T00:00:00Z")},
				{Date: parseTime("2022-10-30T00:00:00Z")},
				{Date: parseTime("2022-10-31T00:00:00Z")},
				{Date: parseTime("2022-11-01T00:00:00Z")},
				{Date: parseTime("2022-11-02T00:00:00Z")},
				{Date: parseTime("2022-11-03T00:00:00Z")},
				{Date: parseTime("2022-11-04T00:00:00Z")},
			},
			StartDate: parseTime("2022-10-22T00:00:00Z"),
			EndDate:   parseTime("2022-11-04T00:00:00Z"),
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, err := c.EpisodeAnalytics(context.Background(), tt.episodeID, tt.query)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}
