package transistor

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"
	"strings"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/flimzy/ale/errors"
	"gitlab.com/flimzy/testy"
)

func parseTime(str string) time.Time {
	t, _ := time.Parse(time.RFC3339, str)
	return t
}

// newResponder returns a *testy.HTTPResponder that serves a single
// response.
func newResponder(t *testing.T, status int, i interface{}) testy.HTTPResponder {
	t.Helper()
	return func(*http.Request) (*http.Response, error) {
		return newResponse(t, status, i), nil
	}
}

// newRequest returns an *http.Response with the provided status, and i
// marshaled as JSON in the body.
func newResponse(t *testing.T, status int, i interface{}) *http.Response {
	t.Helper()
	var body io.Reader
	switch tp := i.(type) {
	case string:
		body = strings.NewReader(tp)
	case []byte:
		body = bytes.NewReader(tp)
	default:
		x, err := json.Marshal(i)
		if err != nil {
			t.Fatal(err)
		}
		body = bytes.NewReader(x)
	}

	return &http.Response{
		StatusCode: status,
		Body:       io.NopCloser(body),
	}
}

func TestNew(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder testy.HTTPResponder
		err       string
		status    int
		want      *User
	}

	tests := testy.NewTable()
	tests.Add("unauthorized", tt{
		responder: newResponder(t, http.StatusUnauthorized, `{"errors":[{"title":"Unauthorized"}]}`),
		err:       "Unauthorized",
		status:    http.StatusUnauthorized,
	})
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusOK,
			`{"data":{"id":"17558","type":"user",
			"attributes":{"created_at":"2021-04-28T18:49:39.879Z",
			"image_url":null,"name":"Jonathan Hall","time_zone":"UTC",
			"updated_at":"2022-09-05T12:08:46.914Z"}}}`),
		want: &User{
			ID:        "17558",
			CreatedAt: parseTime("2021-04-28T18:49:39.879Z"),
			Name:      "Jonathan Hall",
			TimeZone:  "UTC",
			UpdatedAt: parseTime("2022-09-05T12:08:46.914Z"),
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, err := c.User(context.Background())
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}
