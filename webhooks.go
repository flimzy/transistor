package transistor

import (
	"context"
	"encoding/json"
	"net/http"
	"time"
)

// Webhook is an individual subscription to a Transistor webhook.
//
// See https://developers.transistor.fm/#Webhook
type Webhook struct {
	ID        string    `json:"id"`
	CreatedAt time.Time `json:"created_at"`
	EventName string    `json:"event_name"`
	UpdatedAt time.Time `json:"updated_at"`
	URL       string    `json:"url"`
	UserID    string    `json:"-"`
	ShowID    string    `json:"-"`
}

// UnmarshalJSON satisfies the json.Unmarshaler interface.
func (w *Webhook) UnmarshalJSON(p []byte) error {
	target := struct {
		*Webhook
		Attributes    json.RawMessage `json:"attributes"`
		Relationships json.RawMessage `json:"relationships"`
		UnmarshalJSON struct{}        `json:"-"`
	}{
		Webhook: w,
	}
	if err := json.Unmarshal(p, &target); err != nil {
		return err
	}
	if len(target.Attributes) > 0 {
		if err := json.Unmarshal(target.Attributes, &target); err != nil {
			return err
		}
	}
	if len(target.Relationships) > 0 {
		rels := new(relationships)
		if err := json.Unmarshal(target.Relationships, &rels); err != nil {
			return err
		}
		w.ShowID = rels.get("show")[0]
		w.UserID = rels.get("user")[0]
	}
	return nil
}

// Webhooks retrieve a list of webhooks for a show.
//
// See https://developers.transistor.fm/#webhooks
func (c *Client) Webhooks(ctx context.Context, showID string) ([]*Webhook, error) {
	request := struct {
		ShowID string `url:"show_id"`
	}{
		ShowID: showID,
	}
	response := new(struct {
		Data []*Webhook `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodGet, c.path("webhooks").query(request), nil, response)
	return response.Data, err
}

// SubscribeWebhook subscribes to a webhook with the given event name and show.
//
// See https://developers.transistor.fm/#webhooks
func (c *Client) SubscribeWebhook(ctx context.Context, showID, event, url string) (*Webhook, error) {
	request := struct {
		ShowID    string `url:"show_id"`
		EventName string `url:"event_name"`
		URL       string `url:"url"`
	}{
		ShowID:    showID,
		EventName: event,
		URL:       url,
	}
	response := new(struct {
		Data *Webhook `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodPost, c.path("webhooks"), request, response)
	return response.Data, err
}

// UnsubscribeWebhook unsubscribes from the specified webhook.
//
// See https://developers.transistor.fm/#webhooks
func (c *Client) UnsubscribeWebhook(ctx context.Context, webhookID string) (*Webhook, error) {
	response := new(struct {
		Data *Webhook `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodDelete, c.path("webhooks", webhookID), nil, response)
	return response.Data, err
}
