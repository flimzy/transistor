package transistor

import (
	"context"
	"fmt"
	"testing"
)

func TestShowAnalytics_live(t *testing.T) {
	t.Parallel()
	c := liveClient(t)
	got, err := c.ShowAnalytics(context.Background(), testShowID)
	if err != nil {
		t.Fatal(err)
	}
	if len(got.Downloads) > 14 {
		t.Errorf("Expected %d results, got %d", 14, len(got.Downloads))
	}
}

func TestAllEpisodesAnalytics_live(t *testing.T) {
	t.Parallel()
	c := liveClient(t)
	got, err := c.AllEpisodesAnalytics(context.Background(), testShowID)
	if err != nil {
		t.Fatal(err)
	}
	if len(got.Episodes) > 0 {
		fmt.Println(got.Episodes[0])
		t.Errorf("Expected %d results, got %d", 0, len(got.Episodes))
	}
}
