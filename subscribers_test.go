package transistor

import (
	"context"
	"errors"
	"io"
	"net/http"
	"strings"
	"testing"
)

type myTransport func(*http.Request) (*http.Response, error)

func (t myTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	return t(req)
}

func TestSubscribers(t *testing.T) {
	t.Parallel()
	t.Run("public show", func(t *testing.T) {
		t.Parallel()
		c, err := New("", "")
		if err != nil {
			t.Fatal(err)
		}
		c.SetClient(&http.Client{
			Transport: myTransport(func(*http.Request) (*http.Response, error) {
				return &http.Response{
					StatusCode: http.StatusBadRequest,
					Body:       io.NopCloser(strings.NewReader(`{"errors":[{"title":"Public show cannot have subscribers"}]}`)),
				}, nil
			}),
		})
		_, _, err = c.Subscribers(context.Background(), &SubscribersQuery{
			ShowID: testShowID,
		})
		var errMsg string
		if err != nil {
			errMsg = err.Error()
		}
		const wantErr = "Public show cannot have subscribers"
		if errMsg != wantErr {
			t.Errorf("Unexpected error message received: %s", errMsg)
		}
		var httpErr interface{ HTTPStatus() int }
		var status int
		if errors.As(err, &httpErr) {
			status = httpErr.HTTPStatus()
		}
		const wantStatus = http.StatusBadRequest
		if status != wantStatus {
			t.Errorf("Unexpected HTTP status in error: %v", status)
		}
	})
}
