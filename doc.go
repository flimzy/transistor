// Package transistor provides a Go SDK to the transistor.fm JSON:API interface
// described at https://developers.transistor.fm/
package transistor
