package transistor

import (
	"context"
	"net/http"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"gitlab.com/flimzy/ale/errors"
	"gitlab.com/flimzy/testy"
)

const (
	testShowID    = "35376"
	testEpisodeID = "1061187"
)

func TestShows_live(t *testing.T) {
	t.Parallel()
	c := liveClient(t)
	shows, _, err := c.Shows(context.Background(), nil)
	if err != nil {
		t.Fatal(err)
	}
	if len(shows) > 1 {
		t.Errorf("Wanted 1 show, got %d", len(shows))
	}
}

func TestShow_live(t *testing.T) {
	t.Parallel()
	c := liveClient(t)
	shows, err := c.Show(context.Background(), testShowID)
	if err != nil {
		t.Fatal(err)
	}
	if shows.Title != "The Testcast" {
		t.Errorf("Unexpected title: %s", shows.Title)
	}
}

func TestUpdateShow_live(t *testing.T) {
	t.Parallel()
	c := liveClient(t)
	t.Run("success", func(t *testing.T) {
		show := &Show{
			ID:    testShowID,
			Title: "The Testcast",
		}
		show, err := c.UpdateShow(context.Background(), show)
		if err != nil {
			t.Fatal(err)
		}
		want := &Show{
			ID:            testShowID,
			Author:        "Jonathan Hall",
			OwnerEmail:    "jonathan+apitest@jhall.io",
			FeedURL:       "https://feeds.transistor.fm/the-testcast",
			Description:   "Testing only",
			Language:      "en",
			PlaylistLimit: 25,
			Slug:          "the-testcast",
			ShowType:      "episodic",
			Title:         "The Testcast",
			TimeZone:      "Pacific Time (US & Canada)",
			Episodes:      []string{"1061187"},
		}
		if d := cmp.Diff(want, show, cmpopts.IgnoreFields(Show{}, "CreatedAt", "UpdatedAt")); d != "" {
			t.Error(d)
		}
	})
	t.Run("invalid id", func(t *testing.T) {
		t.Parallel()
		show := &Show{
			ID:    "8675309",
			Title: "Jenny",
		}
		_, err := c.UpdateShow(context.Background(), show)
		wantErr := "Resource not found"
		wantStatus := http.StatusNotFound
		if !testy.ErrorMatches(wantErr, err) {
			t.Errorf("Unexpected erorr: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != wantStatus {
			t.Errorf("Unexpected error status: %v", status)
		}
	})
	t.Run("invalid update", func(t *testing.T) {
		t.Parallel()
		show := &Show{
			ID:       testShowID,
			Category: "Parody music",
		}
		_, err := c.UpdateShow(context.Background(), show)
		wantErr := "show[category] does not have a valid value"
		wantStatus := http.StatusUnprocessableEntity
		if !testy.ErrorMatches(wantErr, err) {
			t.Errorf("Unexpected erorr: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != wantStatus {
			t.Errorf("Unexpected error status: %v", status)
		}
	})
}
