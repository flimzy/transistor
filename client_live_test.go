package transistor

import (
	"context"
	"os"
	"testing"
)

// liveClient will return a live client for testing if the
// TRANSISTOR_API_KEY environment variable is set. Otherwise it calls
// t.Skip().
func liveClient(t *testing.T) *Client {
	t.Helper()
	key := os.Getenv("TRANSISTOR_API_KEY")
	if key == "" {
		t.Skip("TRANSITOR_API_KEY not set, skipping live tests")
	}
	c, _ := New("", key)
	return c
}

func Test_unauthorized(t *testing.T) {
	t.Parallel()
	c, _ := New("", "")
	_, err := c.User(context.Background())
	if err == nil || err.Error() != "Unauthorized" {
		t.Errorf("Unexpected error: %s", err)
	}
}

func TestUser(t *testing.T) {
	t.Parallel()
	c := liveClient(t)
	user, err := c.User(context.Background())
	if err != nil {
		t.Fatal(err)
	}
	if user.ID == "" {
		t.Errorf("Expected user to be populated")
	}
}
