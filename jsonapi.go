package transistor

import (
	"encoding/json"
)

// Pagination represents the pagination arguments understood by the
// Transistor.fm API.
type Pagination struct {
	// Page is the page number.
	Page int `url:"page,omitempty"`
	// Per is the number of resources per page.
	Per int `url:"per,omitempty"`
}

// Meta represents metadata about a response.
type Meta struct {
	CurrentPage int `json:"currentPage"`
	TotalPages  int `json:"totalPages"`
	TotalCount  int `json:"totalCount"`
}

type rel struct {
	ID string `json:"id"`
}

type rels []rel

func (r *rels) UnmarshalJSON(p []byte) error {
	if p[0] == '[' {
		var target []rel
		err := json.Unmarshal(p, &target)
		*r = target
		return err
	}
	if len(*r) < 1 {
		*r = make([]rel, 1)
	}
	return json.Unmarshal(p, &(*r)[0])
}

type relationships map[string]relData

type relData struct {
	Data rels `json:"data"`
}

func (r relationships) get(typ string) []string {
	vals := r[typ].Data
	result := make([]string, len(vals))
	for i, val := range vals {
		result[i] = val.ID
	}
	return result
}

// unmarshalRelationships unmarshals the relationshps from p, then extracts
// the relationship specified by key and returns it.
func unmarshalRelationships(p []byte, key string) ([]string, error) {
	if len(p) == 0 {
		return nil, nil
	}
	rels := new(relationships)
	if err := json.Unmarshal(p, &rels); err != nil {
		return nil, err
	}
	return rels.get(key), nil
}
