[run]
timeout = "120s"

[output]
format = "colored-line-number"

[linters]
disable-all = true
enable = [
    "unused", "gofumpt", "goimports", "unconvert", "gosimple", "unparam",
    "ineffassign", "prealloc", "makezero", "misspell", "errname",
    "sqlclosecheck", "noctx", "bodyclose", "gocritic", "govet", "revive",
    "staticcheck", "depguard", "forbidigo", "errcheck", "rowserrcheck",
    "errorlint", "errchkjson", "dupl", "gosec", "exportloopref", "tenv",
    "thelper", "tparallel", "nestif", "usestdlibvars", "nolintlint",
]

[issues]
exclude-use-default = false

[[issues.exclude-rules]]
source = "^//go:generate"
linters = ["revive"]

[[issues.exclude-rules]]
text = "declaration of \"err\" shadows"
linters = ["govet"]

[[issues.exclude-rules]]
path = ".*_test.go"
text = "fieldalignment:"

[[issues.exclude-rules]]
path = ".*_test.go"
source = "tests.Run\\(t, func\\(t \\*testing\\.T, "
linters = ["thelper"]

[[issues.exclude-rules]]
path = ".*_test.go"
source = "tests.Add\\("
linters = ["thelper"]

[[issues.exclude-rules]]
source = "struct{}"
text = "no nested structs are allowed"
linters = ["revive"]

[[issues.exclude-rules]]
text = " is not checked" # This check is duplicated by errcheck, and errcheck is smarter
linters = ["errchkjson"]

[[issues.exclude-rules]]
source = "defer .*\\.Close\\(\\)$"
linters = ["errcheck"]

[linters-settings.gosec]
excludes = [
  "G104", # Duplicate of errcheck
]

[linters-settings.gocritic]
enabled-checks = [
  # To see all available checks, run `GL_DEBUG=gocritic golangci-lint run`

  # Enabled by default
  "appendAssign", "argOrder", "assignOp", "badCall", "badCond", "captLocal",
  "caseOrder", "codegenComment",  "defaultCaseOrder", "deprecatedComment",
  "dupArg", "dupBranchBody", "dupCase", "dupSubExpr", "elseif",
  "exitAfterDefer", "flagDeref", "flagName", "ifElseChain", "mapKey",
  "newDeref", "offBy1", "regexpMust", "singleCaseSwitch", "sloppyLen",
  "sloppyTypeAssert", "switchTrue", "typeSwitchVar", "underef", "unlambda",
  "unslice", "valSwap", "wrapperFunc",
  # "commentFormatting", # functionality provided by gofumpt

  # Non-default checks we want
  "unnecessaryDefer", "ioutilDeprecated", "commentedOutCode", "appendCombine",
  "paramTypeCombine", "hugeParam",

  # These are disabled by default, but might be valuable
  # "badLock", "badRegexp", "badSorting", "boolExprSimplify", "builtinShadow",
  # "builtinShadowDecl", "commentedOutImport", "deferInLoop", "deferUnlambda",
  # "docStub", "dupImport", "dynamicFmtString", "emptyDecl", "emptyFallthrough",
  # "emptyStringTest", "equalFold", "evalOrder", "exposedSyncMutex",
  # "externalErrorReassign", "filepathJoin", "hexLiteral", "httpNoBody",
  # "indexAlloc", "initClause", "methodExprCall",
  # "nestingReduce", "nilValReturn", "octalLiteral", "preferDecodeRune",
  # "preferFilepathJoin", "preferFprint", "preferStringWriter",
  # "preferWriteByte", "ptrToRefParam", "rangeExprCopy", "rangeValCopy",
  # "redundantSprint", "regexpPattern", "regexpSimplify",
  # "returnAfterHttpError", "ruleguard", "sliceClear", "sloppyReassign",
  # "sortSlice", "sprintfQuotedString", "sqlQuery", "stringConcatSimplify",
  # "stringXbytes", "stringsCompare", "syncMapLoadAndDelete",
  # "timeExprSimplify", "todoCommentWithoutDetail", "tooManyResultsChecker",
  # "truncateCmp", "typeAssertChain", "typeDefFirst", "typeUnparen",
  # "unlabelStmt", "unnamedResult", "unnecessaryBlock", "weakCond",
  # "yodaStyleExpr"

  # These are disabled because we explicitly don't want them
  # "importShadow"
  # "whyNoLint", # nolintlint does a better job
]

[linters-settings.prealloc]
for-loops = true

[linters-settings.nolintlint]
allow-unused = false
allow-leading-space = false
require-explanation = true
require-specific = true

[linters-settings.errcheck]
check-type-assertions = true

[linters-settings.govet]
check-shadowing = true
enable-all = true
disable = ["fieldalignment"]

[linters-settings.forbidigo]
forbid = [
  "^log.Fatal*$",
  "^log.Panic*$",
]
exclude_godoc_examples = false

[linters-settings.revive]
max-open-files = 2_048
ignore-generated-header = true
severity = "error"
enable-all-rules = true
confidence = 0.1

  [[linters-settings.revive.rules]]
  name = "add-constant"
  disabled = true

    [[linters-settings.revive.rules.arguments]]
    maxLitCount = "3"
    allowStrs = '""'
    allowInts = "0,1,2"
    allowFloats = "0.0,0.,1.0,1.,2.0,2."

  [[linters-settings.revive.rules]]
  name = "argument-limit"
  disabled = false
  arguments = [ 10 ]

  [[linters-settings.revive.rules]]
  name = "atomic"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "banned-characters"
  disabled = true

  [[linters-settings.revive.rules]]
  name = "bare-return"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "blank-imports"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "bool-literal-in-expr"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "call-to-gc"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "cognitive-complexity"
  disabled = true

  [[linters-settings.revive.rules]]
  name = "confusing-naming"
  disabled = true

  [[linters-settings.revive.rules]]
  name = "confusing-results"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "constant-logical-expr"
  severity = "warning"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "context-as-argument"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "context-keys-type"
  disabled = true # Duplicate of staticcheck

  [[linters-settings.revive.rules]]
  name = "cyclomatic"
  disabled = true
  arguments = [ 16 ]

  [[linters-settings.revive.rules]]
  name = "deep-exit"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "defer"
  disabled = false
  arguments = [ [ "call-chain", "loop" ] ]

  [[linters-settings.revive.rules]]
  name = "dot-imports"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "duplicated-imports"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "early-return"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "empty-block"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "empty-lines"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "error-naming"
  severity = "warning"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "error-return"
  severity = "warning"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "error-strings"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "errorf"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "exported"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "file-header"
  disabled = true

  [[linters-settings.revive.rules]]
  name = "flag-parameter"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "function-result-limit"
  disabled = false
  arguments = [ 3 ]

  [[linters-settings.revive.rules]]
  name = "function-length"
  disabled = true

  [[linters-settings.revive.rules]]
  name = "get-return"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "identical-branches"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "if-return"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "increment-decrement"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "indent-error-flow"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "imports-blacklist"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "import-shadowing"
  disabled = true

  [[linters-settings.revive.rules]]
  name = "line-length-limit"
  disabled = false
  arguments = [ 200 ]

  [[linters-settings.revive.rules]]
  name = "max-public-structs"
  disabled = true

  [[linters-settings.revive.rules]]
  name = "modifies-parameter"
  disabled = true

  [[linters-settings.revive.rules]]
  name = "modifies-value-receiver"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "nested-structs"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "optimize-operands-order"
  disabled = true # too many wrong suggestions

  [[linters-settings.revive.rules]]
  name = "package-comments"
  disabled = true # Buggy, complains about every file

  [[linters-settings.revive.rules]]
  name = "range"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "range-val-in-closure"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "range-val-address"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "receiver-naming"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "redefines-builtin-id"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "string-of-int"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "string-format"
  disabled = true

  [[linters-settings.revive.rules]]
  name = "struct-tag"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "superfluous-else"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "time-equal"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "time-naming"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "var-naming"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "var-declaration"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "unconditional-recursion"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "unexported-naming"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "unexported-return"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "unhandled-error"
  disabled = true

  [[linters-settings.revive.rules]]
  name = "unnecessary-stmt"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "unreachable-code"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "unused-parameter"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "unused-receiver"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "useless-break"
  severity = "warning"
  disabled = false

  [[linters-settings.revive.rules]]
  name = "waitgroup-by-value"
  disabled = false

[linters-settings.usestdlibvars]
http-method = true
http-status-code = true
time-weekday = true
time-month = true
time-layout = true
crypto-hash = true
default-rpc-path = true
