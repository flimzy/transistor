package transistor

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"mime"
	"net/http"
	"net/url"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/flimzy/ale/errors"
	"gitlab.com/flimzy/urlquery"
)

// DefaultURL is the default URL. It may be overridden with Client#SetURL.
const DefaultURL = "https://api.transistor.fm/v1"

// Client represents a client connection to the Transistor.fm API.
type Client struct {
	base   *url.URL
	key    string
	client *http.Client
	debug  bool
}

// New returns a new Transistor.fm API client instance. If baseURL is omitted,
// DefaultURL is used.
func New(baseURL, apiKey string) (*Client, error) {
	if baseURL == "" {
		baseURL = DefaultURL
	}
	base, err := url.Parse(baseURL)
	if err != nil {
		return nil, err
	}
	return &Client{
		base:   base,
		key:    apiKey,
		client: &http.Client{},
	}, nil
}

// SetClient sets a custom HTTP client.
func (c *Client) SetClient(client *http.Client) {
	c.client = client
}

type path struct {
	params interface{}
	base   *url.URL
	parts  []string
}

func (p *path) query(i interface{}) *path {
	p.params = i
	return p
}

func (p *path) String() (string, error) {
	p.base.Path = filepath.Join(append([]string{p.base.Path}, p.parts...)...)
	if p.params == nil {
		return p.base.String(), nil
	}
	if params, ok := p.params.(url.Values); ok {
		p.base.RawQuery = params.Encode()
		return p.base.String(), nil
	}
	params, err := urlquery.JSONAPI().Encode(p.params)
	if err != nil {
		return "", err
	}
	p.base.RawQuery = params.Encode()
	return p.base.String(), nil
}

func (c *Client) path(parts ...string) *path {
	// make a copy
	base := new(url.URL)
	*base = *c.base
	return &path{
		base:  base,
		parts: parts,
	}
}

func (c *Client) doRequest(ctx context.Context, method string, path *path, query, target interface{}) error {
	var body io.Reader
	var ct string
	switch method {
	case http.MethodHead, http.MethodGet:
	default:
		if query != nil {
			values, err := urlquery.JSONAPI().Encode(query)
			if err != nil {
				return err
			}
			encoded := values.Encode()
			if c.debug {
				fmt.Printf("request body: %s\n", encoded)
			}
			body = strings.NewReader(values.Encode())
			ct = "application/x-www-form-urlencoded"
		}
	}
	reqPath, err := path.String()
	if err != nil {
		return err
	}
	if c.debug {
		fmt.Printf("Request path: %s\n", reqPath)
	}
	req, err := http.NewRequestWithContext(ctx, method, reqPath, body)
	if err != nil {
		return err
	}
	if ct != "" {
		req.Header.Set("Content-Type", ct)
	}
	req.Header.Set("X-Api-Key", c.key)
	res, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	if c.debug {
		bodyBytes, err := io.ReadAll(res.Body)
		if err != nil {
			return err
		}
		fmt.Printf("[%v] response body: %s\n", res.StatusCode, string(bodyBytes))
		res.Body = io.NopCloser(bytes.NewReader(bodyBytes))
	}
	if res.StatusCode >= http.StatusBadRequest {
		return parseError(res)
	}
	return json.NewDecoder(res.Body).Decode(target)
}

func parseError(res *http.Response) error {
	if ct, _, _ := mime.ParseMediaType(res.Header.Get("Content-Type")); ct == "text/plain" {
		text, err := io.ReadAll(res.Body)
		if err != nil {
			return err
		}
		return &Error{Title: string(text), status: res.StatusCode}
	}
	var payload struct {
		Errors []*Error `json:"errors"`
		// Some error statuses don't return a JSON:API formatted message, so
		// this should catch those.
		Error string `json:"error"`
	}
	if e := json.NewDecoder(res.Body).Decode(&payload); e != nil {
		return e
	}
	var err error
	for _, e := range payload.Errors {
		e.status = res.StatusCode
		err = errors.Append(err, error(e))
	}
	if payload.Error != "" {
		err = errors.Append(err, &Error{Title: payload.Error, status: res.StatusCode})
	}
	return err
}

// Error represents an API error.
//
// This is a copy of jsonapi.ErrorObject, with minor customizations.
type Error struct {
	// status is the actual HTTP status included in the response.
	status int
	// ID is a unique identifier for this particular occurrence of a problem.
	ID string `json:"id,omitempty"`
	// Title is a short, human-readable summary of the problem that SHOULD NOT change from occurrence to occurrence of the problem, except for purposes of localization.
	Title string `json:"title,omitempty"`
	// Detail is a human-readable explanation specific to this occurrence of the problem. Like title, this field’s value can be localized.
	Detail string `json:"detail,omitempty"`
	// Status is the HTTP status code applicable to this problem.
	Status string `json:"status,omitempty"`
	// Code is an application-specific error code, expressed as a string value.
	Code string `json:"code,omitempty"`
	// Meta is an object containing non-standard meta-information about the error.
	Meta *map[string]interface{} `json:"meta,omitempty"`
}

func (e *Error) Error() string {
	if e.Detail != "" {
		return e.Title + " " + e.Detail
	}
	return e.Title
}

// HTTPStatus returns the HTTP status of the response that returned this
// error.
func (e *Error) HTTPStatus() int {
	return e.status
}

// User represents a Transistor.fm user.
type User struct {
	ID        string    `json:"id"`
	CreatedAt time.Time `json:"created_at"`
	ImageURL  *string   `json:"image_url"`
	Name      string    `json:"name"`
	TimeZone  string    `json:"time_zone"`
	UpdatedAt time.Time `json:"updated_at"`
}

// UnmarshalJSON satisfies the json.Unmarshaler interface.
func (u *User) UnmarshalJSON(p []byte) error {
	target := struct {
		*User
		Attributes    json.RawMessage `json:"attributes"`
		UnmarshalJSON struct{}        `json:"-"`
	}{
		User: u,
	}
	if err := json.Unmarshal(p, &target); err != nil {
		return err
	}
	if len(target.Attributes) == 0 {
		return nil
	}
	return json.Unmarshal(target.Attributes, &target)
}

// User retrieves details of the user account that is authenticating to the
// API.
//
// See https://developers.transistor.fm/#root
func (c *Client) User(ctx context.Context) (*User, error) {
	response := new(struct {
		Data *User `json:"data"`
	})
	err := c.doRequest(ctx, http.MethodGet, c.path(), nil, response)
	return response.Data, err
}
