package transistor

import (
	"context"
	"net/http"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/flimzy/ale/errors"
	"gitlab.com/flimzy/testy"
)

func TestWebhooks(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder testy.HTTPResponder
		showID    string
		err       string
		status    int
		want      []*Webhook
	}

	tests := testy.NewTable()
	tests.Add("success, but no results", tt{
		responder: newResponder(t, http.StatusOK, `{"data":[]}`),
		want:      []*Webhook{},
	})
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusOK, `{"data":[{"id":"997","type":"webhook",
		"attributes":{"event_name":"episode_created","url":"http://example.com/",
		"created_at":"2022-10-16T12:14:09.089Z","updated_at":"2022-10-16T12:14:09.089Z"},
		"relationships":{"user":{"data":{"id":"28546","type":"user"}},"show":{"data":{"id":"35376","type":"show"}}}},
		{"id":"998","type":"webhook","attributes":{"event_name":"episode_created","url":"http://example.com/",
		"created_at":"2022-10-16T12:14:11.048Z","updated_at":"2022-10-16T12:14:11.048Z"},
		"relationships":{"user":{"data":{"id":"28546","type":"user"}},"show":{"data":{"id":"35376","type":"show"}}}}]}`),
		want: []*Webhook{
			{
				ID:        "997",
				CreatedAt: parseTime("2022-10-16T12:14:09.089Z"),
				UpdatedAt: parseTime("2022-10-16T12:14:09.089Z"),
				EventName: "episode_created",
				URL:       "http://example.com/",
				UserID:    "28546",
				ShowID:    "35376",
			},
			{
				ID:        "998",
				CreatedAt: parseTime("2022-10-16T12:14:11.048Z"),
				UpdatedAt: parseTime("2022-10-16T12:14:11.048Z"),
				EventName: "episode_created",
				URL:       "http://example.com/",
				UserID:    "28546",
				ShowID:    "35376",
			},
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, err := c.Webhooks(context.Background(), tt.showID)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}

func TestSubscribeWebhook(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder          testy.HTTPResponder
		showID, event, url string
		err                string
		status             int
		want               *Webhook
	}

	tests := testy.NewTable()
	tests.Add("invalid event type", tt{
		responder: newResponder(t, http.StatusUnprocessableEntity, `{"errors":[{"title":"event_name does not have a valid value"}]}`),
		showID:    testShowID,
		event:     "oink",
		err:       "event_name does not have a valid value",
		status:    http.StatusUnprocessableEntity,
	})
	tests.Add("missing URL", tt{
		responder: newResponder(t, http.StatusUnprocessableEntity, `{"errors":[{"title":"Url can't be blank"}]}`),
		showID:    testShowID,
		event:     "episode_created",
		err:       "Url can't be blank",
		status:    http.StatusUnprocessableEntity,
	})
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusCreated, `{"data":{"id":"998","type":"webhook",
		"attributes":{"event_name":"episode_created","url":"http://example.com/",
		"created_at":"2022-10-16T12:14:11.048Z","updated_at":"2022-10-16T12:14:11.048Z"},
		"relationships":{"user":{"data":{"id":"28546","type":"user"}},"show":{"data":{"id":"35376","type":"show"}}}}}`),
		showID: testShowID,
		event:  "episode_created",
		url:    "http://example.com/",
		want: &Webhook{
			ID:        "998",
			EventName: "episode_created",
			CreatedAt: parseTime("2022-10-16T12:14:11.048Z"),
			UpdatedAt: parseTime("2022-10-16T12:14:11.048Z"),
			URL:       "http://example.com/",
			UserID:    "28546",
			ShowID:    testShowID,
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, err := c.SubscribeWebhook(context.Background(), tt.showID, tt.event, tt.url)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}

func TestUnsubscribeWebhook(t *testing.T) {
	t.Parallel()
	type tt struct {
		responder testy.HTTPResponder
		webhookID string
		err       string
		status    int
		want      *Webhook
	}

	tests := testy.NewTable()
	tests.Add("success", tt{
		responder: newResponder(t, http.StatusCreated, `{"data":{"id":"1001","type":"webhook",
		"attributes":{"event_name":"episode_created","url":"http://example.com/",
		"created_at":"2022-10-16T12:23:50.798Z","updated_at":"2022-10-16T12:23:50.798Z"},
		"relationships":{"user":{"data":{"id":"28546","type":"user"}},"show":{"data":{"id":"35376","type":"show"}}}}}`),
		webhookID: "1001",
		want: &Webhook{
			ID:        "1001",
			EventName: "episode_created",
			CreatedAt: parseTime("2022-10-16T12:23:50.798Z"),
			UpdatedAt: parseTime("2022-10-16T12:23:50.798Z"),
			URL:       "http://example.com/",
			UserID:    "28546",
			ShowID:    testShowID,
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()
		c, _ := New("", "")
		c.SetClient(testy.HTTPClient(tt.responder))

		got, err := c.UnsubscribeWebhook(context.Background(), tt.webhookID)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}
